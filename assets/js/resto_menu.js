let MENU_ID,
    IMG_URL

$(document).ready(function(){
  getCategory()
  //data table
  let menu_table = $('#menu-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${RESTO_MENU_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          let column = d.order[0].column
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          newObj.sort = d.order[0].dir
          switch (column) {
              case 1:
                  newObj.order_by = 'name'
                  break
              case 2:
                  newObj.order_by = 'type'
                  break
              case 3:
                  newObj.order_by = 'price'
                  break
              case 4:
                  newObj.order_by = 'created_at'
                  break
              default:
                  newObj.order_by = 'name'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 1, "asc" ]],
      columns: [
          { 
            data: "full_img_url",
            orderable: false,
            render: function (data, type, row, meta) {
              return `<img src="${data}" style="height:100px;">`
            }
          },
          { 
            data: "name"
          },
          { 
            data: "type"
          },
          { 
            data: "price",
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              return formatAUD(data)
            }
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            }
          },
          {
            data: "id",
            orderable: false,
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let button = `<div class="btn-group" role="group" aria-label="Basic example">
                <button class="btn btn-sm btn-success menu-update-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#menu-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger menu-delete-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#menu-delete-modal" title="delete">
                  <i class="fas fa-trash"></i>
                </button>
              </div>`
              
              return button
            }
          }
      ]
  });
  
  //toggle
  $('#menu-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".menu-update-toggle", "click", function(e) {
    clearForm('update')
    MENU_ID = $(this).data('id')
    $('#menu-update-overlay').show()

    $.ajax({
        async: true,
        url: `${RESTO_MENU_API_URL}/by-id/${MENU_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#menu-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
          $('#menu-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".menu-delete-toggle", "click", function(e) {
    MENU_ID = $(this).data('id')
  })

  $('#menu-create-image').change(function(e) {
    $('#menu-create-button').attr("disabled", true)
    let file = e.target.files[0];
    let formData = new FormData();
    formData.append('image', file);

    // AJAX upload file
    $.ajax({
      url: `${RESTO_MENU_API_URL}/upload-image`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        let data = response.data
        console.log('File uploaded successfully: ' + data);
        IMG_URL = data.img_url
        $('#menu-create-button').attr("disabled", false)
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Upload gambar", "Gagal melakukan upload gambar!")
        $('#menu-create-button').attr("disabled", false)
      }
    });
  });

  //form
  $('#menu-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#menu-create-button')
    
    let $form = $(this),
        request = {
          img_url: IMG_URL,
          name: $form.find( "input[name='name']" ).val(),
          category_id: $('#menu-create-category').find(":selected").val(),
          price: $form.find( "input[name='price']" ).val(),
          type: $('#menu-create-type').find(":selected").val(),
          description: $form.find( "textarea[name='description']" ).val(),
        }
        
    $.ajax({
        async: true,
        url: RESTO_MENU_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#menu-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#menu-create-button', 'Submit')
          $('#menu-create-modal').modal('hide')
          menu_table.ajax.reload()
        }
    });
  })

  $('#menu-update-image').change(function(e) {
    $('#menu-update-button').attr("disabled", true)
    let file = e.target.files[0];
    let formData = new FormData();
    formData.append('image', file);

    // AJAX upload file
    $.ajax({
      url: `${RESTO_MENU_API_URL}/upload-image`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        let data = response.data
        console.log('File uploaded successfully: ' + data);
        IMG_URL = data.img_url
        $('#menu-update-button').attr("disabled", false)
        $('#menu-update-image-section').html(`<img src="${data.full_img_url}" style="height:100px;">`)
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Upload gambar", "Gagal melakukan upload gambar!")
        $('#menu-update-button').attr("disabled", false)
      }
    });
  });
  
  $('#menu-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#menu-update-button')

    let $form = $(this),
        request = {
          id: MENU_ID,
          img_url: IMG_URL,
          name: $form.find( "input[name='name']" ).val(),
          resto_category_id: $('#menu-update-category').find(":selected").val(),
          price: $form.find( "input[name='price']" ).val(),
          type: $('#menu-update-type').find(":selected").val(),
          description: $form.find( "textarea[name='description']" ).val(),
        }

    $.ajax({
        async: true,
        url: RESTO_MENU_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#menu-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#menu-update-button', 'Submit')
          $('#menu-update-modal').modal('toggle')
          menu_table.ajax.reload()
        }
    });
  })
  
  $('#menu-delete-button').click(function (){
    startLoadingButton('#menu-delete-button')
    
    $.ajax({
        async: true,
        url: `${RESTO_MENU_API_URL}/delete/${MENU_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#menu-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#menu-delete-button', 'Submit')
          $('#menu-delete-modal').modal('toggle')
          menu_table.ajax.reload()
        }
    });
  })
})

function getCategory(){
  $.ajax({
    async: true,
    url: `${RESTO_CATEGORY_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderCategory(data)
    }
  });
}

function renderCategory(items){
  let categories_html = ''
  items.forEach(item => {
    let item_html = `<option value="${item.id}">${item.name}</option>`
    categories_html += item_html
  });

  $('.category-option').html(categories_html)
}

function renderForm(data){
  IMG_URL = data.img_url
  let $form = $(`#menu-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='price']" ).val(data.price)
  $form.find( "textarea[name='description']" ).val(data.description)
  $(`#menu-update-category`).val(data.resto_category_id).change()
  $(`#menu-update-type`).val(data.type).change()
  $('#menu-update-image-section').html(`<img src="${data.full_img_url}" style="height:100px;">`)
}

function clearForm(type){
  let $form = $(`#menu-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "input[name='price']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
}