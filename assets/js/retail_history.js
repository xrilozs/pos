let HISTORY_ID,
    DATE = formatDateStr(),
    PAYMENT_TYPE = ""

$(document).ready(function(){
  $('#history-datepicker').datepicker({
    autoclose: true // Close the datepicker when a date is selected
  });
  $('#history-datepicker').datepicker("update", new Date());

  //data table
  let history_table = $('#history-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${RETAIL_HISTORY_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          let column = d.order[0].column
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.payment_type = PAYMENT_TYPE
          newObj.date = DATE
          newObj.draw = d.draw
          newObj.sort = d.order[0].dir
          switch (column) {
              case 0:
                  newObj.order_by = 'order_number'
                  break
              case 1:
                  newObj.order_by = 'created_at'
                  break
              case 2:
                  newObj.order_by = 'payment_type'
                  break
              case 3:
                  newObj.order_by = 'total'
                  break
              case 5:
                  newObj.order_by = 'created_at'
                  break
              case 6:
                  newObj.order_by = 'status'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 1, "asc" ]],
      columns: [
          { 
            data: "order_number"
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDatetimeID(data)
            }
          },
          { 
            data: "payment_type"
          },
          { 
            data: "total",
            render: function (data, type, row, meta) {
              return formatAUD(data)
            }
          },
          { 
            data: "items",
            render: function (data, type, row, meta) {
              let items_html = ``
              data.forEach(item => {
                items_html += `<p><span class="badge bg-secondary">${item.item_name}: ${item.qty} qty</span></p>`
              })
              return items_html
            },
            orderable: false
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return getUnixMillis(data)
            }
          },
          {
            data: "status",
            render: function (data, type, row, meta) {
              let color = 'secondary'
              if(data == 'PAID') color = 'success'
              if(data == 'REFUND') color = 'warning'
              return `<span class="badge bg-${color}">${data}</span>`
            }
          },
          {
            data: "id",
            orderable: false,
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let refund_disabled = row.status == 'REFUND' ? 'disabled' : ''
              let button = `<div class="btn-group" role="group">
                <button class="btn btn-sm btn-warning history-refund-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#history-refund-modal" title="refund" ${refund_disabled}>
                  <i class="fas fa-hand-holding-usd"></i>
                </button>
              </div>`
              
              return button
            },
          }
      ]
  });

  $('#history-payment-type').change(function(){
    PAYMENT_TYPE = $(this).val()
  })

  $('#history-date').change(function(){
    DATE = formatFromDatePicker($(this).val())
  })

  $('#history-button').click(function(){
    history_table.ajax.reload()
  })
  
  //toggle
  $('#history-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".history-update-toggle", "click", function(e) {
    clearForm('update')
    HISTORY_ID = $(this).data('id')
    $('#history-update-overlay').show()

    $.ajax({
        async: true,
        url: `${RETAIL_HISTORY_API_URL}/by-id/${HISTORY_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#history-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
          $('#history-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".history-refund-toggle", "click", function(e) {
    HISTORY_ID = $(this).data('id')
  })

  //form  
  $('#history-refund-button').click(function (){
    startLoadingButton('#history-refund-button')
    
    $.ajax({
        async: true,
        url: `${RETAIL_ORDER_API_URL}refund/${HISTORY_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#history-refund-button', 'Iya')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#history-refund-button', 'Iya')
          $('#history-refund-modal').modal('toggle')
          history_table.ajax.reload()
        }
    });
  })
})
