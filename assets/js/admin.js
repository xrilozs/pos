let ADMIN_ID

$(document).ready(function(){
  //data table
  let admin_table = $('#admin-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${ADMIN_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj  = {}
          let start   = d.start
          let size    = d.length
          let column  = d.order[0].column
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.sort         = d.order[0].dir
          switch (column) {
              case 0:
                  newObj.order_by = 'name'
                  break
              case 1:
                  newObj.order_by = 'pin'
                  break
              case 2:
                  newObj.order_by = 'role'
                  break
              case 3:
                  newObj.order_by = 'created_at'
                  break
              default:
                  newObj.order_by = 'name'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "name"
          },
          { 
            data: "pin",
            render: function (data, type, row, meta) {
              return "*****"
            }
          },
          { 
            data: "phone",
            render: function (data, type, row, meta) {
              return data ? data : "-"
            }
          },
          { 
            data: "email",
            render: function (data, type, row, meta) {
              return data ? data : "-"
            }
          },
          { 
            data: "role",
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let color = data == 'ADMIN' ? 'primary' : 'secondary'
              let badge = `<span class="badge bg-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            }
          },
          {
            data: "id",
            orderable: false,
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let button = `<div class="btn-group" role="group" aria-label="Basic example">
                <button class="btn btn-sm btn-success admin-update-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#admin-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger admin-delete-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#admin-delete-modal" title="delete">
                  <i class="fas fa-trash"></i>
                </button>
              </div>`
              
              return button
            }
          }
      ]
  });
  
  //toggle
  $('#admin-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".admin-update-toggle", "click", function(e) {
    clearForm('update')
    ADMIN_ID = $(this).data('id')
    $('#admin-update-overlay').show()

    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}/by-id/${ADMIN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#admin-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
          $('#admin-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".admin-delete-toggle", "click", function(e) {
    ADMIN_ID = $(this).data('id')
  })

  //form
  $('#admin-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#admin-create-button')
    
    let $form = $(this),
        request = {
          name: $form.find( "input[name='name']" ).val(),
          pin: $form.find( "input[name='pin']" ).val(),
          phone: $form.find( "input[name='phone']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          role: $('#admin-create-role').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-create-button', 'Submit')
          $('#admin-create-modal').modal('hide')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#admin-update-button')

    let $form = $(this),
        request = {
          id: ADMIN_ID,
          name: $form.find( "input[name='name']" ).val(),
          pin: $form.find( "input[name='pin']" ).val(),
          phone: $form.find( "input[name='phone']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          role: $('#admin-update-role').find(":selected").val()
        }

    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-update-button', 'Submit')
          $('#admin-update-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-delete-button').click(function (){
    startLoadingButton('#admin-delete-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}delete/${ADMIN_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-delete-button', 'Submit')
          $('#admin-delete-modal').modal('toggle')
          admin_table.ajax.reload()
        }
    });
  })
})

function renderForm(data){
  let $form = $(`#admin-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='pin']" ).val(data.pin)
  $form.find( "input[name='phone']" ).val(data.phone)
  $form.find( "input[name='email']" ).val(data.email)
  $(`#admin-update-role`).val(data.role).change()
}

function clearForm(type){
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "input[name='pin']" ).val("")
  $form.find( "input[name='phone']" ).val("")
  $form.find( "input[name='email']" ).val("")
}