let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION         = localStorage.getItem("user-token");
let REFRESH_SESSION = localStorage.getItem("user-refresh-token")

$(document).ready(function(){
  if(SESSION){ 
    window.location.href = 'resto/point-of-sales'
  }

  $("#login-form").submit(function(e) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form = $( this ),
        pin   = $form.find( "input[name='pin']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}/admin/login`,
        type: 'POST',
        data: JSON.stringify({
          pin: pin
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Masuk')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          window.location.href = 'resto/point-of-sales'
        }
    });    
  })
})

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
}