let DISCOUNT_ID

$(document).ready(function(){
  //data table
  let discount_table = $('#discount-datatable').DataTable( {
    processing: true,
    serverSide: true,
    searching: true,
    ajax: {
      async: true,
      url: `${RETAIL_DISCOUNT_API_URL}`,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function ( d ) {
        let newObj = {}
        let start = d.start
        let size = d.length
        let column = d.order[0].column
        newObj.page_number = d.start > 0 ? (start/size) : 0;
        newObj.page_size = size
        newObj.search = d.search.value
        newObj.draw = d.draw
        newObj.sort = d.order[0].dir
        switch (column) {
            case 0:
                newObj.order_by = 'name'
                break
            case 1:
                newObj.order_by = 'type'
                break
            case 2:
                newObj.order_by = 'value'
                break
            case 3:
                newObj.order_by = 'created_at'
                break
            default:
                newObj.order_by = 'name'
                break
        }
        d = newObj
        console.log("D itu:", d)
        return d
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry     = retryRequest(response)
        if(isRetry) $.ajax(this)
      }
    },
    order: [[ 0, "asc" ]],
    columns: [
        { 
          data: "name"
        },
        { 
          data: "type"
        },
        { 
          data: "value",
          render: function (data, type, row, meta) {
            if(row.type == 'AMOUNT'){
              return formatAUD(data, true)
            }else{
              return `${data} %`
            }
          }
        },
        {
          data: "created_at",
          render: function (data, type, row, meta) {
            return formatDateID(data)
          }
        },
        {
          data: "id",
          orderable: false,
          className: "dt-body-center",
          render: function (data, type, row, meta) {
            let button = `<div class="btn-group" role="group" aria-label="Basic example">
              <button class="btn btn-sm btn-success discount-update-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#discount-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger discount-delete-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#discount-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>
            </div>`
            
            return button
          }
        }
    ]
  });

  //change
  $('#discount-create-type').change(function(){
    const val = $(this).val()
    if(val == 'AMOUNT'){
      $('#nominal-create').show()
      $('#percentage-create').hide()
      $('#discount-create-nominal').attr("required", true)
      $('#discount-create-percentage').attr("required", false)
    }else{
      $('#nominal-create').hide()
      $('#percentage-create').show()
      $('#discount-create-nominal').attr("required", false)
      $('#discount-create-percentage').attr("required", true)
    }
  })

  $('#discount-update-type').change(function(){
    const val = $(this).val()
    if(val == 'AMOUNT'){
      $('#nominal-update').show()
      $('#percentage-update').hide()
      $('#discount-update-nominal').attr("required", true)
      $('#discount-update-percentage').attr("required", false)
    }else{
      $('#nominal-update').hide()
      $('#percentage-update').show()
      $('#discount-update-nominal').attr("required", false)
      $('#discount-update-percentage').attr("required", true)
    }
  })

  //toggle
  $('#discount-create-toggle').click(function(e) {
    clearForm('create')  
  })

  $("body").delegate(".discount-update-toggle", "click", function(e) {
    clearForm('update')
    DISCOUNT_ID = $(this).data('id')

    $.ajax({
        async: true,
        url: `${RETAIL_DISCOUNT_API_URL}/by-id/${DISCOUNT_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#discount-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
        }
    });
  })

  $("body").delegate(".discount-delete-toggle", "click", function(e) {
    DISCOUNT_ID = $(this).data('id')
  })

  //form
  $('#discount-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#discount-create-button')
    const type =  $('#discount-create-type').find(":selected").val()
    const value = type == 'AMOUNT' ? $('#discount-create-nominal').val() : $('#discount-create-percentage').val()
    
    let $form = $(this),
        request = {
          name: $form.find( "input[name='name']" ).val(),
          type: type,
          value: value,
        }
        
    $.ajax({
        async: true,
        url: RETAIL_DISCOUNT_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#discount-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#discount-create-button', 'Submit')
          $('#discount-create-modal').modal('hide')
          discount_table.ajax.reload()
        }
    });
  })

  $('#discount-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#discount-update-button')
    const type =  $('#discount-update-type').find(":selected").val()
    const value = type == 'AMOUNT' ? $('#discount-update-nominal').val() : $('#discount-update-percentage').val()

    let $form = $(this),
        request = {
          id: DISCOUNT_ID,
          name: $form.find( "input[name='name']" ).val(),
          type: type,
          value: value,
        }

    $.ajax({
        async: true,
        url: RETAIL_DISCOUNT_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#discount-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#discount-update-button', 'Submit')
          $('#discount-update-modal').modal('toggle')
          discount_table.ajax.reload()
        }
    });
  })

  $('#discount-delete-button').click(function (){
    startLoadingButton('#discount-delete-button')
    
    $.ajax({
        async: true,
        url: `${RETAIL_DISCOUNT_API_URL}delete/${DISCOUNT_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#discount-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#discount-delete-button', 'Submit')
          $('#discount-delete-modal').modal('toggle')
          discount_table.ajax.reload()
        }
    });
  })
})


function renderForm(data){
  let $form = $(`#discount-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $(`#discount-update-type`).val(data.type).change()
  $form.find( "input[name='value']" ).val(data.value)
}

function clearForm(type){
  console.log(`${type} form clear`)
  let $form = $(`#discount-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $(`#discount-${type}-type`).val("AMOUNT").change()
  $form.find( "input[name='value']" ).val("")
}