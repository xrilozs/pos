let CATEGORY_ID

$(document).ready(function(){
  //data table
  let category_table = $('#category-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${RESTO_CATEGORY_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          let column = d.order[0].column
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          newObj.sort = d.order[0].dir
          switch (column) {
              case 0:
                  newObj.order_by = 'name'
                  break
              case 1:
                  newObj.order_by = 'description'
                  break
              case 2:
                  newObj.order_by = 'sort_no'
                  break
              case 3:
                  newObj.order_by = 'created_at'
                  break
              default:
                  newObj.order_by = 'name'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 2, "asc" ]],
      columns: [
          { 
            data: "name"
          },
          { 
            data: "description",
            render: function (data, type, row, meta) {
              return data ? sortText(data) : "-"
            }
          },
          { 
            data: "sort_no"
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            }
          },
          {
            data: "id",
            orderable: false,
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let button = `<div class="btn-group" role="group" aria-label="Basic example">
                <button class="btn btn-sm btn-success category-update-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#category-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger category-delete-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#category-delete-modal" title="delete">
                  <i class="fas fa-trash"></i>
                </button>
              </div>`
              
              return button
            }
          }
      ]
  });
  
  //toggle
  $('#category-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".category-update-toggle", "click", function(e) {
    clearForm('update')
    CATEGORY_ID = $(this).data('id')
    $('#category-update-overlay').show()

    $.ajax({
        async: true,
        url: `${RESTO_CATEGORY_API_URL}/by-id/${CATEGORY_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#category-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
          $('#category-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".category-delete-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
  })

  //form
  $('#category-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#category-create-button')
    
    let $form = $(this),
        request = {
          name: $form.find( "input[name='name']" ).val(),
          sort_no: $form.find( "input[name='sort_no']" ).val(),
          description: $form.find( "textarea[name='description']" ).val()
        }
        
    $.ajax({
        async: true,
        url: RESTO_CATEGORY_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#category-create-button', 'Submit')
          $('#category-create-modal').modal('hide')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#category-update-button')

    let $form = $(this),
        request = {
          id: CATEGORY_ID,
          name: $form.find( "input[name='name']" ).val(),
          sort_no: $form.find( "input[name='sort_no']" ).val(),
          description: $form.find( "textarea[name='description']" ).val(),
        }

    $.ajax({
        async: true,
        url: RESTO_CATEGORY_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#category-update-button', 'Submit')
          $('#category-update-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-delete-button').click(function (){
    startLoadingButton('#category-delete-button')
    
    $.ajax({
        async: true,
        url: `${RESTO_CATEGORY_API_URL}delete/${CATEGORY_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#category-delete-button', 'Submit')
          $('#category-delete-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
})

function renderForm(data){
  let $form = $(`#category-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='sort_no']" ).val(data.sort_no)
  $form.find( "textarea[name='description']" ).val(data.description)
}

function clearForm(type){
  let $form = $(`#category-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "input[name='sort_no']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
}