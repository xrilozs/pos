let START_DATE,
    END_DATE

$(document).ready(function(){
  $('#start-datepicker').datepicker();
  $('#end-datepicker').datepicker();

  $('#report-start-date').change(function(){
    START_DATE = formatFromDatePicker($(this).val())
    console.log(START_DATE)
    if(START_DATE && END_DATE){
      $('#report-button').attr("disabled", false)
    }
  })

  $('#report-end-date').change(function(){
    END_DATE = formatFromDatePicker($(this).val())
    console.log(END_DATE)
    if(START_DATE && END_DATE){
      $('#report-button').attr("disabled", false)
    }
  })

  $('#report-button').click(function(){
    get_retail_report()
  })
})

function get_retail_report(){
  startLoadingButton("#report-button")
  let queryParam = ""
  if(START_DATE){
    queryParam += `?start_date=${START_DATE}`
  }
  if(END_DATE){
    queryParam += START_DATE ? "&" : "?"
    queryParam += `end_date=${END_DATE}`
  }
  $.ajax({
    async: true,
    url: `${RETAIL_REPORT_API_URL}${queryParam}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      let isRetry     = retryRequest(response)
      if(isRetry) $.ajax(this)
      else showError("Gagal menampilkan report")
      endLoadingButton('#report-button', 'SHOW')
    },
    success: function(res) {
      // showSuccess(res.message)
      endLoadingButton('#report-button', 'SHOW')
      renderReport(res.data)
    }
  });
}

function renderReport(data){
  $('#report-section').show()

  let report_date = "DATE "
  if(START_DATE) report_date += START_DATE
  if(END_DATE) {
    if(START_DATE) report_date += " - "
    report_date += END_DATE
  }

  $("#report-date").html(report_date)
  $('#report-total-sales').html(formatAUD(data.total_sales))
  $('#report-total-order').html(data.total_order)
  $('#report-total-discount').html(formatAUD(data.discount_total))
  // $('#report-total-tax').html()
  $('#report-total-cash').html(formatAUD(data.cash_payment))
  $('#report-total-cc').html(formatAUD(data.cc_payment))
}