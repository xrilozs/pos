let CATEGORY_ID, SEARCH,
  SELECTED_MENU = [],
  SELECTED_MENU_ID,
  SHOWING_MENU = [],
  SELECTED_DISCOUNT_TEMP,
  SELECTED_DISCOUNT,
  SUBTOTAL = 0,
  DISCOUNT = 0,
  GRAND_TOTAL = 0
  SEARCH = ""
// let isSelecting = 0;
// let isSearching = 0;
let isClosing   = 0;
let closeCount  = 0;
let SELECTED_ITEM;
let search_param = ''
  
$(document).ready(function(){
  getCategory()
  getItem()

  //POS Item Search
  // $('#pos-menu-search').select2({
  //   width: '100%',
  //   selectOnClose: false,
  //   closeOnSelect:true,
  //   placeholder: "Cari menu..",
  //   minimumInputLength: 1,
  //   language: {
  //     inputTooShort: function(args) {
  //       return "Cari barang..";
  //     },
  //     noResults: function(){
  //       return "Barang tidak ditemukan";
  //     }
  //   },
  //   ajax: {
  //     url: `${RETAIL_ITEM_API_URL}?draw=1&sort=ASC&order_by=name`,
  //     beforeSend: function (xhr) {
  //       xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
  //     },
  //     data: function (params) {
  //       var query = {
  //         search: params.term.trim(),
  //         page_size: 10,
  //         page_number: 0
  //       }
  //       search_param = params.term.trim()
  
  //       return query;
  //     },
  //     processResults: function (response) {
  //       console.log("SEARCHING..")
  //       let menu = response.data
  //       if(menu.length > 0) {
  //         menu = menu.map(item => {
  //           item.text = `(${item.barcode}) ${item.name}`
  //           return item
  //         })

  //           console.log("LENGTH: ", menu.length)
  //           console.log("BARCODE SEARCH: ", search_param)
  //           console.log("BARCODE ITEM: ", menu[0].barcode)
  //           if(menu.length == 1 && search_param == menu[0].barcode){
  //             SELECTED_ITEM = menu[0]
  //             add_to_cart()
  //             $('#pos-menu-search').select2('close');
  //           }
  //       }
  //       // Transforms the top-level key of the response object from 'items' to 'results'
  //       return {
  //         results: menu
  //       };
  //     }
  //   }
  // });
  // $('#pos-menu-search').select2('open');
  // document.querySelector('.select2-search__field').focus();
  // $('#pos-menu-search').data('select2').$selection.css('height', '60px');
  // $('#pos-menu-search').on("select2:selecting", function(e) {
  //     SELECTED_ITEM = e.params.args.data
  //     console.log("SELECTING..")
  //     add_to_cart()
  // });
  // $('#pos-menu-search').on("select2:opening", function(e) {
  //   $('#pos-menu-search').val("").change();
  // })
  // $('#pos-menu-search').on('select2:open', function (e) {
  //     document.querySelector('.select2-search__field').focus();
  // });

  $('#menu-search').on("input", function(){
    SEARCH = $(this).val();
    console.log(SEARCH)
    getItem()
  });

  //POS Discount Search
  $('#discount-search').select2({
    width: '100%',
    placeholder: "Cari diskon..",
    dropdownParent: $("#discount-model"),
    selectOnClose: true,
    ajax: {
      url: `${RETAIL_DISCOUNT_API_URL}active`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        var query = {
          search: params.term,
        }
  
        return query;
      },
      processResults: function (response) {
        let discount = response.data
        discount = discount.map(item => {
          let value = item.type == 'AMOUNT' ? formatAUD(item.value) : `${item.value}%`
          item.text = `(${value}) ${item.name}`
          // item.text = `(${item.code}) ${item.name}`
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: discount
        };
      }
    }
  });
  $('#discount-search').data('select2').$selection.css('height', '60px');
  $('#discount-search').on("select2:selecting", function(e) { 
    const selected_data = e.params.args.data
    console.log(selected_data)
    SELECTED_DISCOUNT_TEMP = selected_data
  });
  $('#discount-toggle').click(function(){
    if(SELECTED_DISCOUNT){
      $('#discount-search').val(SELECTED_DISCOUNT.id).change();

    }
  })

  //POS Item Menu
  $("body").delegate(".item-menu", "click", function(e) {
    let selected_data = $(this).data("object")
    console.log(selected_data)
    let menuIndex = SELECTED_MENU.findIndex(item => item.id == selected_data.id)
    if(menuIndex < 0){
      selected_data.qty = 1
      SELECTED_MENU.push(selected_data)
    }else{
      SELECTED_MENU[menuIndex].qty = parseInt(SELECTED_MENU[menuIndex].qty) + 1
    }

    renderSelectedMenu()
  })

  //POS Item Category
  $("body").delegate(".item-category", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    getItem()
  })

  $("body").delegate("#all-category", "click", function(e) {
    CATEGORY_ID = null
    getItem()
  })
    

  $('#discount-form').submit(function(e){
    e.preventDefault()
    SELECTED_DISCOUNT = SELECTED_DISCOUNT_TEMP
    renderSelectedMenu()
    
    $('#discount-model').modal("toggle")
  })

  $('#edit-item-form').submit(function(e){
    e.preventDefault()
    SELECTED_MENU[SELECTED_MENU_ID].qty = $('#edit-item-qty').val()
    renderSelectedMenu()
    
    $('#edit-item-model').modal("toggle")
  })

  //Clear item button
  $('#clear-items-button').click(function(){
    SELECTED_MENU = []
    renderSelectedMenu()
    $("#clear-items-model").modal('toggle')
  })
  $('#clear-item-button').click(function(){
    SELECTED_MENU.splice(SELECTED_MENU_ID, 1)
    renderSelectedMenu()
    $("#clear-item-model").modal('toggle')
  })
  $("body").delegate(".clear-item-toggle", "click", function(e) {
    SELECTED_MENU_ID = $(this).data("id")
  })

  $("body").delegate(".edit-item-toggle", "click", function(e) {
    SELECTED_MENU_ID = $(this).data("id")
    renderEditItemForm()
  })

  $("body").delegate(".edit-item-qty-pos", "change", function(e) {
    let qty = $(this).val()
    SELECTED_MENU_ID = $(this).data("id")
    SELECTED_MENU[SELECTED_MENU_ID].qty = qty
    renderSelectedMenu()
  })

  $('#credit-card-form').submit(function(e){
    e.preventDefault()

    $('#credit-card-button').attr("disabled", true)
    let request = {
      discount: DISCOUNT,
      subtotal: SUBTOTAL,
      total_pay: GRAND_TOTAL,
      payment_type: "CREDIT CARD",
      order_items: []
    }

    SELECTED_MENU.forEach(menu => {
      let item = {
        retail_item_id: menu.id,
        qty: menu.qty,
        price: menu.price,
        name: menu.name
      }

      request.order_items.push(item)
    })

    // AJAX upload file
    $.ajax({
      url: `${RETAIL_ORDER_API_URL}`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: JSON.stringify(request),
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        pc_print(response)
        // let data = response.data
        // console.log('success' + data);
        showSuccess("Pembayaran Credit Card", "Berhasil melakukan pembayaran melalui credit card!")

        SELECTED_MENU = []
        renderSelectedMenu()
        $('#credit-card-button').attr("disabled", false)
        $('#credit-card-modal').modal('toggle')
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Pembayaran Credit Card", "Gagal melakukan pembayaran melalui credit card!")
        $('#credit-card-button').attr("disabled", false)
        $('#credit-card-modal').modal('toggle')
      }
    });
  })

  $('#cash-form').submit(function(e){
    e.preventDefault()

    $('#cash-button').attr("disabled", true)
    let request = {
      discount: DISCOUNT,
      subtotal: SUBTOTAL,
      total_pay: $('#cash-total-pay').val(),
      payment_type: "CASH",
      order_items: []
    }

    SELECTED_MENU.forEach(menu => {
      let item = {
        retail_item_id: menu.id,
        qty: menu.qty,
        price: menu.price,
        name: menu.name
      }

      request.order_items.push(item)
    })

    // AJAX upload file
    $.ajax({
      url: `${RETAIL_ORDER_API_URL}`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: JSON.stringify(request),
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        // let data = response.data
        // console.log('success' + data);
        pc_print(response)

        showSuccess("Pembayaran Cash", "Berhasil melakukan pembayaran melalui cash!")
        SELECTED_MENU = []
        renderSelectedMenu()
        $('#cash-button').attr("disabled", false)
        $('#cash-modal').modal('toggle')
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Pembayaran Cash", "Gagal melakukan pembayaran melalui cash!")
        $('#cash-button').attr("disabled", false)
        $('#cash-modal').modal('toggle')
      }
    });
  })

  $('#cash-total-pay').on("input", function(){
    let val = $(this).val()
    let exchange = parseFloat(val) - GRAND_TOTAL
    $('#cash-exchange').val(formatAUD(exchange))
  })
})

//AJAX
function getCategory(){
  $.ajax({
    async: true,
    url: `${RETAIL_CATEGORY_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderCategory(data)
    }
  });
}
function getItem(){
  let url = `${RETAIL_ITEM_API_URL}?sort=ASC&order_by=name`
  if(CATEGORY_ID) url += `&retail_category_id=${CATEGORY_ID}`
  else if(SEARCH) url += `&search=${SEARCH}`
  else url += `&page_size=120&page_number=0`
  // if(SEARCH) url += `&search=${SEARCH}`

  $.ajax({
    async: true,
    url: url,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderMenu(data)
    }
  });
}

//Render
function renderCategory(items){
  let items_html = `<button
    class="btn btn-outline-secondary w-100" type="button" id="all-category"
    style="min-height:60px;margin-top:10px;margin-bottom:5px;word-wrap:break-word;"
  >
    ALL
  </button>`
  items.forEach(item => {
    let item_html = `<button
      class="btn btn-outline-secondary w-100 item-category" type="button" data-id="${item.id}"
      style="height:62px; margin-top:10px; margin-bottom:5px; word-wrap:break-word; font-size:14px;"
    >
      ${item.name}
    </button>`
    items_html += item_html
  });

  $('#pos-category').html(items_html)
}
function renderMenu(menus){
  let items_html = ""
  menus.forEach(item => {
    let isFullUrl = false 
    if(item.img_url && item.img_url.includes("https://")) isFullUrl = true
    else if(item.img_url && item.img_url.includes("http://")) isFullUrl = true
    let image_html = item.img_url ? `<img
      src="${isFullUrl ? item.img_url : item.full_img_url}"
      class="card-img-top" style="padding: 5px;"
      alt="Gambar ${item.name}"
    />` : "-no image yet-"
    let item_html = `<div class="col-lg-3 col-md-4 col-sm-2">
      <div class="card text-center border-2 my-2 item-menu" style="cursor: pointer; height: 130px;" data-object='${escapeHtmlChars(JSON.stringify(item))}'>
        ${image_html}
        <div class="">
          <p style="font-size:12px; padding:0px;">${sortText(item.name, 20)}</p> 
        </div>
      </div>
    </div>`
    items_html += item_html
  });

  $('.menu-harga').html(items_html)
}
function renderSelectedMenu(){
  if(SELECTED_MENU.length > 0){
    $('#clear-items-toggle').attr("disabled", false)
    let item_total = 0
    SUBTOTAL = 0
    SELECTED_MENU.forEach(menu => {
      item_total = parseInt(item_total) + parseInt(menu.qty)
      SUBTOTAL += menu.price * menu.qty
    })
    $('#item-total').html(item_total)
    $('#item-subtotal').html(formatAUD(SUBTOTAL))

    DISCOUNT = 0
    GRAND_TOTAL = SUBTOTAL - DISCOUNT
    if(SELECTED_DISCOUNT){
      DISCOUNT = SELECTED_DISCOUNT.type == 'AMOUNT' ? SELECTED_DISCOUNT.value : SUBTOTAL * (SELECTED_DISCOUNT.value/100)
      GRAND_TOTAL = SUBTOTAL - DISCOUNT
    }
    $('#item-discount').html(formatAUD(DISCOUNT))
    $('#item-grandtotal').html(formatAUD(GRAND_TOTAL))
    $('#cash-total').val(formatAUD(GRAND_TOTAL))
  }else{
    SUBTOTAL = 0
    DISCOUNT = 0
    GRAND_TOTAL = 0
    $('#clear-items-toggle').attr("disabled", true)
    $('#item-total').html(0)
    $('#item-subtotal').html(formatAUD(0))
    $('#item-grandtotal').html(formatAUD(GRAND_TOTAL))
    $('#cash-total').val(formatAUD(GRAND_TOTAL))
    if(SELECTED_DISCOUNT){
      $('#item-discount').html(formatAUD(0))
    }
  }

  let menu_html = `<tr>
    <td style="width:35%">Item</td>
    <td style="text-align: right;width:35%;">Qty</td>
    <td style="text-align: right;width:15%;">Price</td> 
    <td style="text-align: right;width:15%;">Amount</td> 
    <td style="width:20%;">Action</td>
  </tr>`
  SELECTED_MENU.forEach((item, id) => {
    let item_html = `<tr>
      <td style="width:30%;">${item.name}</td>
      <td style="text-align: right;width:20%;">
        <input type="number" min="1" class="form-control edit-item-qty-pos" data-id="${id}" value="${item.qty ? item.qty : 1}" style="padding:6px 2px;">
      </td>
      <td style="text-align: right;width:20%;">${formatAUD(item.price)}</td> 
      <td style="text-align: right;width:20%;">${formatAUD(item.price * item.qty)}</td> 
      <td style="width:30%;">
        <!--<button class="btn btn-secondary edit-item-toggle" data-id="${id}" data-bs-toggle="modal" data-bs-target="#edit-item-model">edit</button>-->
        <button class="btn btn-danger clear-item-toggle" data-id="${id}" data-bs-toggle="modal" data-bs-target="#clear-item-model">X</button>
      </td>
    </tr>`
    menu_html += item_html
  });
  $('#pos-selected-menu').html(menu_html)
}
function renderEditItemForm(){
  const editItem = SELECTED_MENU[SELECTED_MENU_ID]
  console.log(editItem)
  $('#edit-item-name').val(editItem.name)
  $('#edit-item-qty').val(editItem.qty)
}

function add_to_cart(){
  let menuIndex = SELECTED_MENU.findIndex(item => item.id == SELECTED_ITEM.id)
  if(menuIndex < 0){
    SELECTED_ITEM.qty = 1
    SELECTED_MENU.push(SELECTED_ITEM)
  }else{
    SELECTED_MENU[menuIndex].qty = parseInt(SELECTED_MENU[menuIndex].qty) + 1
  }
  console.log("SELECTING")
  console.log("SELECTED: ", SELECTED_ITEM)

  renderSelectedMenu()
}

function pc_print(data){
  var socket = new WebSocket("ws://127.0.0.1:40213/");
  socket.bufferType = "arraybuffer";
  socket.onerror = function(error) {
    alert("Error");
  };			
  socket.onopen = function() {
      socket.send(data);
      socket.close(1000, "Work complete");
  };
}
// function ajax_print(url) {
//   $.get(url, function (data) {
//       pc_print(data);
//   });
// }