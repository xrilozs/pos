let SESSION                   = localStorage.getItem("user-token"),
    REFRESH_SESSION           = localStorage.getItem("user-refresh-token"),
    ADMIN_API_URL             = API_URL + "/admin/",
    RESTO_CATEGORY_API_URL    = API_URL + "/resto-category/",
    RESTO_DISCOUNT_API_URL    = API_URL + "/resto-discount/",
    RESTO_MENU_API_URL        = API_URL + "/resto-menu/",
    RESTO_ORDER_API_URL       = API_URL + "/resto-menu-order/",
    RESTO_ORDER_ITEM_API_URL  = API_URL + "/resto-menu-order-item/",
    RETAIL_CATEGORY_API_URL   = API_URL + "/retail-category/",
    RETAIL_DISCOUNT_API_URL   = API_URL + "/retail-discount/",
    RETAIL_ITEM_API_URL       = API_URL + "/retail-item/",
    RETAIL_ORDER_API_URL      = API_URL + "/retail-order/",
    RETAIL_ORDER_ITEM_API_URL = API_URL + "/retail-order-item/",
    RETAIL_REPORT_API_URL     = API_URL + "/retail-order/report",
    RETAIL_HISTORY_API_URL    = API_URL + "/retail-order/history",
    RETRY_COUNT               = 0,
    PROFILE

$(document).ready(function(){
  $('#logout-button').click(function(){
    removeSession()
  })

  $(".price-field").change(function() {
    let value = $(this).val();

    // Remove non-numeric characters (except dot)
    value = value.replace(/[^0-9.]/g, '');

    // If there are multiple dots, keep only the first one
    value = value.replace(/\.(?=.*\.)/g, '');
    console.log(value)
    value = value ? value : 0

    // Convert to a floating-point number with two decimal places
    let formattedValue = parseFloat(value).toFixed(2);

    // Update the input field value with the formatted number
    $(this).val(formattedValue);
  });

  if(SESSION){
    getProfile()
  }else{
    window.location.href = WEB_URL + "/login"
  }
})

/* API */
function getProfile(){
  $.ajax({
      async: true,
      url: `${ADMIN_API_URL}profile`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        const response  = JSON.parse(res.responseText)
        let isRetry     = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        PROFILE = res.data
        renderProfile(res.data)
        renderMainMenu()
      }
  });
}
/* API */

/* Render */
function renderProfile(data){
  $('.account-name').html(`Welcome, ${data.name}`)
}

function renderMainMenu(){
  if(PROFILE.role == 'STAFF'){
    $('.non-staff-menu').hide()
  }
}
/* Render */

/* Animation */
function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(title, text, confirmLink=null){
  Swal.fire({
      icon: 'error',
      title: title,
      text: text,
      confirmButtonColor: '#6c63ff'
  }).then((result) => {
      if (confirmLink) {
          window.location.href = confirmLink
      }
  }); 
}

function showSuccess(title, text, confirmLink=null){
  Swal.fire({
      icon: 'success',
      title: title,
      text: text,
      confirmButtonColor: '#6c63ff'
  }).then((result) => {
      if (confirmLink) {
          window.location.href = confirmLink
      }
  }); 
}
/* Animation */

/* Session */
function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
}

function removeSession(source=null){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  if(source) window.location.href = `${WEB_URL}/login?source=${source}`
  else window.location.href       = `${WEB_URL}/login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${ADMIN_API_URL}refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        // const response  = JSON.parse(res.responseText)
        resp            = {status: "failed"}
      },
      success: function(res) {
        const response  = res.data
        resp            = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError, source){
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()
      console.log("REFRESH: ", resObj)
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession(source)
      }
    }else{
      removeSession(source)
    }
  }else{
    showError(responseError.message)
    return false
  }
}
/* Session */


/* Format */
function formatRupiah(angka, prefix){
  let angkaStr  = angka.replace(/[^,\d]/g, '').toString(),
      split     = angkaStr.split(','),
      sisa      = split[0].length % 3,
      rupiah    = split[0].substr(0, sisa),
      ribuan    = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatAUD(angka){
  return "$"+parseFloat(angka).toFixed(2)
}

function formatDateID(date_str=null){

  let day_arr   = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
  let month_arr = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

  let datetime  = date_str ? new Date(date_str) : new Date(),
      day       = day_arr[datetime.getDay()],
      date      = datetime.getDate(),
      month     = month_arr[datetime.getMonth()],
      year      = datetime.getFullYear()

  return `${day}, ${date} ${month} ${year}`
}

function formatDatetimeID(date_str=null){

  let day_arr   = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
  let month_arr = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

  let datetime  = date_str ? new Date(date_str) : new Date(),
      minute    = datetime.getMinutes(),
      hour      = datetime.getHours(),
      day       = day_arr[datetime.getDay()],
      date      = datetime.getDate(),
      month     = month_arr[datetime.getMonth()],
      year      = datetime.getFullYear()

  let minute_str  = minute > 10 ? minute.toString() : `0${minute.toString()}`
  let hour_str    = hour > 10 ? hour.toString() : `0${hour.toString()}`

  return `${day}, ${date} ${month} ${year}, jam ${hour_str}:${minute_str}`
}

function formatFromDatePicker(date_str){
  let arr = date_str.split("/")
  return `${arr[2]}-${arr[0]}-${arr[1]}`
}

function formatToDatePicker(date_str){
  let arr = date_str.split("-")
  console.log("BEFORE ",date_str)
  console.log("AFTER ", `${arr[1]}/${arr[2]}/${arr[0]}`)
  return `${arr[1]}/${arr[2]}/${arr[0]}`
}

function formatDateStr(date_str=null){
  let datetime  = date_str ? new Date(date_str) : new Date(),
      date      = datetime.getDate(),
      month     = datetime.getMonth()+1,
      year      = datetime.getFullYear()

  return `${year}-${month}-${date}`
}

function getUnixMillis(date_str){
  let datetime  = date_str ? new Date(date_str) : new Date();
  return datetime.valueOf()
}

function sortText(text, size=30){
  return text.length > size ? text.slice(0, size) + ".." : text
}

function escapeHtmlChars(str) {
  return str.replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;');
}
/* Format */

/* Common */
function getQueryParams(){
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params          = Object.fromEntries(urlSearchParams.entries());
  return params
}
/* Common */
