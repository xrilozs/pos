let ITEM_ID,
    IMG_URL,
    PAGE_NUMBER = 0

$(document).ready(function(){
  getCategory()
  //data table
  let item_table = $('#item-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${RETAIL_ITEM_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          let column = d.order[0].column
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          newObj.sort = d.order[0].dir
          switch (column) {
              case 1:
                  newObj.order_by = 'name'
                  break
              case 2:
                  newObj.order_by = 'stock_qty'
                  break
              case 2:
                  newObj.order_by = 'barcode'
                  break
              case 4:
                  newObj.order_by = 'price'
                  break
              case 5:
                  newObj.order_by = 'retail_category_name'
                  break
              case 6:
                  newObj.order_by = 'expired_date'
                  break
              default:
                  newObj.order_by = 'name'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 1, "asc" ]],
      columns: [
          { 
            data: "full_img_url",
            orderable: false,
            render: function (data, type, row, meta) {
              let isFullUrl = false 
              if(row.img_url && row.img_url.includes("https://")) isFullUrl = true
              else if(row.img_url && row.img_url.includes("http://")) isFullUrl = true
              return row.img_url ? `<img src="${isFullUrl ? row.img_url : data}" style="height:100px;">` : "-"
            }
          },
          { 
            data: "name"
          },
          { 
            data: "stock_qty"
          },
          { 
            data: "barcode"
          },
          { 
            data: "price",
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              return formatAUD(data)
            }
          },
          { 
            data: "retail_category_name"
          },
          {
            data: "expired_date",
            render: function (data, type, row, meta) {
              if(data){
                const d1 = new Date();
                const d2 = new Date(data);

                // Calculate the difference in milliseconds
                const diffMs = d2 - d1;

                // Convert the difference in milliseconds to months
                const monthsDifference = diffMs / (1000 * 60 * 60 * 24 * 30.44); // Approximate days in a month

                if(monthsDifference <= 2){
                  let str = monthsDifference <= 0 ? "Sudah Expired" : "Segera Expired";
                  let color = monthsDifference <= 0 ? "danger" : "warning";
                  return `<p>${formatDateID(data)}</p> <span class="badge bg-${color}">${str}</span>`
                }else{
                  return formatDateID(data)
                }
              }else{
                return "-"
              }
            }
          },
          {
            data: "id",
            orderable: false,
            className: "dt-body-center",
            render: function (data, type, row, meta) {
              let button = `<div class="btn-group" role="group" aria-label="Basic example">
                <button class="btn btn-sm btn-success item-update-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#item-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger item-delete-toggle" data-id="${data}" data-bs-toggle="modal" data-bs-target="#item-delete-modal" title="delete">
                  <i class="fas fa-trash"></i>
                </button>
              </div>`
              
              return button
            }
          }
      ]
  });
  
  //toggle
  $('#item-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".item-update-toggle", "click", function(e) {
    clearForm('update')
    ITEM_ID = $(this).data('id')

    $.ajax({
        async: true,
        url: `${RETAIL_ITEM_API_URL}/by-id/${ITEM_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#item-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response)
        }
    });
  })
  
  $("body").delegate(".item-delete-toggle", "click", function(e) {
    ITEM_ID = $(this).data('id')
  })

  $('#item-create-image').change(function(e) {
    $('#item-create-button').attr("disabled", true)
    let file = e.target.files[0];
    let formData = new FormData();
    formData.append('image', file);

    // AJAX upload file
    $.ajax({
      url: `${RETAIL_ITEM_API_URL}/upload-image`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        let data = response.data
        console.log('File uploaded successfully: ' + data);
        IMG_URL = data.img_url
        $('#item-create-button').attr("disabled", false)
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Upload gambar", "Gagal melakukan upload gambar!")
        $('#item-create-button').attr("disabled", false)
      }
    });
  });

  //form
  $('#item-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#item-create-button')
    
    let $form = $(this),
        request = {
          img_url: IMG_URL,
          name: $form.find( "input[name='name']" ).val(),
          retail_category_id: $('#item-create-category').find(":selected").val(),
          price: $form.find( "input[name='price']" ).val(),
          barcode: $form.find( "input[name='barcode']" ).val(),
          stock_qty: $form.find( "input[name='stock_qty']" ).val(),
          description: $form.find( "textarea[name='description']" ).val(),
          expired_date: formatFromDatePicker($form.find( "input[name='expired_date']" ).val()),
        }
        
    $.ajax({
        async: true,
        url: RETAIL_ITEM_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#item-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#item-create-button', 'Submit')
          $('#item-create-modal').modal('hide')
          // item_table.ajax.reload()
          item_table.draw()
        }
    });
  })

  $('#item-update-image').change(function(e) {
    $('#item-update-button').attr("disabled", true)
    let file = e.target.files[0];
    let formData = new FormData();
    formData.append('image', file);

    // AJAX upload file
    $.ajax({
      url: `${RETAIL_ITEM_API_URL}/upload-image`, // Ganti dengan URL upload file PHP Anda
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      success: function(response) {
        let data = response.data
        console.log('File uploaded successfully: ' + data);
        IMG_URL = data.img_url
        $('#item-update-button').attr("disabled", false)
        $('#item-update-image-section').html(`<img src="${data.full_img_url}" style="height:100px;">`)
      },
      error: function(xhr, status, error) {
        // Handle error
        console.log('Error uploading file: ' + error);
        showError("Upload gambar", "Gagal melakukan upload gambar!")
        $('#item-update-button').attr("disabled", false)
      }
    });
  });
  
  $('#item-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#item-update-button')

    let $form = $(this),
        request = {
          id: ITEM_ID,
          img_url: IMG_URL,
          name: $form.find( "input[name='name']" ).val(),
          retail_category_id: $('#item-update-category').find(":selected").val(),
          price: $form.find( "input[name='price']" ).val(),
          barcode: $form.find( "input[name='barcode']" ).val(),
          stock_qty: $form.find( "input[name='stock_qty']" ).val(),
          description: $form.find( "textarea[name='description']" ).val(),
          expired_date: formatFromDatePicker($form.find( "input[name='expired_date']" ).val()),
        }

    $.ajax({
        async: true,
        url: RETAIL_ITEM_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#item-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#item-update-button', 'Submit')
          $('#item-update-modal').modal('toggle')
          // item_table.ajax.reload()
          item_table.draw(false)
        }
    });
  })
  
  $('#item-delete-button').click(function (){
    startLoadingButton('#item-delete-button')
    
    $.ajax({
        async: true,
        url: `${RETAIL_ITEM_API_URL}/delete/${ITEM_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#item-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#item-delete-button', 'Submit')
          $('#item-delete-modal').modal('toggle')
          item_table.ajax.reload()
        }
    });
  })
})

function getCategory(){
  $.ajax({
    async: true,
    url: `${RETAIL_CATEGORY_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderCategory(data)
    }
  });
}

function renderCategory(items){
  let categories_html = ''
  items.forEach(item => {
    let item_html = `<option value="${item.id}">${item.name}</option>`
    categories_html += item_html
  });

  $('.category-option').html(categories_html)
}

function renderForm(data){
  IMG_URL = data.img_url
  let isFullUrl = false 
  if(IMG_URL && IMG_URL.includes("https://")) isFullUrl = true
  else if(IMG_URL && IMG_URL.includes("http://")) isFullUrl = true
  let $form = $(`#item-update-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='price']" ).val(data.price)
  $form.find( "input[name='barcode']" ).val(data.barcode)
  $form.find( "input[name='stock_qty']" ).val(data.stock_qty)
  $form.find( "textarea[name='description']" ).val(data.description)
  $("#item-update-expired-date").datepicker('setDate', formatToDatePicker(data.expired_date));
  $(`#item-update-category`).val(data.retail_category_id).change()
  $(`#item-update-type`).val(data.type).change()
  $('#item-update-image-section').html(IMG_URL ? `<img src="${isFullUrl ? IMG_URL : data.full_img_url}" style="height:100px;">` : 'no image yet')
}

function clearForm(type){
  let $form = $(`#item-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "input[name='price']" ).val("")
  $form.find( "input[name='barcode']" ).val("")
  $form.find( "input[name='stock_qty']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
  $(`#item-${type}-expired-date`).datepicker('setDate', new Date());
}