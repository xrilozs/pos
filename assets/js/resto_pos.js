let CATEGORY_ID, SEARCH,
  SELECTED_MENU = [],
  SHOWING_MENU = []

$(document).ready(function(){
  getCategory()
  getMenu()

  $("body").delegate(".menu-category", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    getMenu()
  })

  let select_menu = $('#pos-menu-search').select2({
    width: '100%',
    placeholder: "Cari menu..",
    ajax: {
      url: `${RESTO_MENU_API_URL}?sort=ASC&order_by=name`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        var query = {
          search: params.term,
          page_size: 10,
          page_number: 0
        }
  
        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (response) {
        let menu = response.data
        menu = menu.map(item => {
          item.text = item.name
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: menu
        };
      }
    }
  });

  select_menu.data('select2').$selection.css('height', '60px');
  select_menu.on("select2:selecting", function(e) { 
    const selected_data = e.params.args.data
    console.log(selected_data)
    let menuExist = SELECTED_MENU.find(item => item.id == selected_data.id)
    if(!menuExist){
      SELECTED_MENU.push(selected_data)
    }
    renderSelectedMenu()
    // $('#potongan_kasbon_create').val(selected_data.potongan_kasbon)
  });
})

function getCategory(){
  $.ajax({
    async: true,
    url: `${RESTO_CATEGORY_API_URL}/all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderCategory(data)
    }
  });
}

function getMenu(){
  let url = `${RESTO_MENU_API_URL}?page_size=12&page_number=0&sort=ASC&order_by=name`
  if(CATEGORY_ID) url += `&category_id=${CATEGORY_ID}`
  if(SEARCH) url += `&search=${SEARCH}`

  $.ajax({
    async: true,
    url: url,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let data = res.data
      renderMenu(data)
    }
  });
}

function renderCategory(items){
  let items_html = ""
  items.forEach(item => {
    let item_html = `<button
      class="btn btn-outline-secondary w-100 menu-category" type="button" data-id="${item.id}"
      style="height:60px;margin-top:10px;margin-bottom:5px;"
    >
      ${item.name}
    </button>`
    items_html += item_html
  });

  $('#pos-category').html(items_html)
}

function renderMenu(menus){
  let items_html = ""
  menus.forEach(item => {
    let item_html = `<div class="col-3">
      <div class="card text-center border-2 my-2">
        <img
          src="${item.full_img_url}"
          class="card-img-top" style="padding: 5px;"
          alt="Gambar ${item.name}"
        />
        <div class="">
          <p style="font-size:80%;padding:5px;">${item.name}</p> 
        </div>
      </div>
    </div>`
    items_html += item_html
  });

  $('.menu-harga').html(items_html)
}

function renderSelectedMenu(){
  let menu_html = `<tr>
    <td style="width:40%">Item</td>
    <td style="text-align: right;width:10%;">Qty</td>
    <td style="text-align: right;width:20%;">Price</td> 
    <td style="width:30%;"><button class="btn" style="color:white">edit</button>
    <button class="btn" style="color:white">X</button></td>
  </tr>`
  SELECTED_MENU.forEach(item => {
    let item_html = `<tr>
      <td style="width:40%;">${item.name}</td>
      <td style="text-align: right;width:10%;">${item.qty ? item.qty : 1}</td>
      <td style="text-align: right;width:20%;">${item.price}</td> 
      <td style="width:30%;"><button class="btn btn-secondary">edit</button>
      <button class="btn btn-danger">X</button></td>
    </tr>`
    menu_html += item_html
  });
  $('#pos-selected-menu').html(menu_html)
}