<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>PRINT</h1>
    <button onclick="onClick()">print</button>
</body>
<script src="https://cdn.jsdelivr.net/npm/recta/dist/recta.js"></script>
<script type="text/javascript">
    // using CommonJS
    // var Recta = require('recta');
    var printer = new Recta('1234567890', '1811')

    function onClick () {
        console.log("printing..")
        printer.open().then(function () {
        printer.align('center')
            .text('Hello World !!')
            .bold(true)
            .text('This is bold text')
            .bold(false)
            .underline(true)
            .text('This is underline text')
            .underline(false)
            .barcode('CODE39', '123456789')
            .cut()
            .print()
        })
    }
</script>
</html>