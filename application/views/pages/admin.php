    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Daftar Staff</h1>
          </div>
          <div class="col-6 d-flex justify-content-end">
            <button 
              class="rounded-pill btn btn-success w-50" 
              data-bs-toggle="modal" data-bs-target="#admin-create-modal"
            >
              <i class="fas fa-plus"></i> BUAT
            </button>
          </div>
        </div>
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
            <table class="table" id="admin-datatable">
              <thead>
                <tr>
                  <th scope="col">Nama</th>
                  <th scope="col">PIN</th>
                  <th scope="col">Telepon</th>
                  <th scope="col">Email</th>
                  <th scope="col">Role</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

<div class="modal" tabindex="-1" id="admin-create-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Staff</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="admin-create-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="admin-create-name" placeholder="Masukan nama staff.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">PIN</label>
          <input type="number" name="pin" class="form-control" id="admin-create-pin" placeholder="Masukan PIN.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Telepon (Opsional)</label>
          <input type="text" name="phone" class="form-control" id="admin-create-phone" placeholder="Masukan nomor telepon..">
        </div>
        <div class="mb-3">
          <label class="form-label">Email (Opsional)</label>
          <input type="text" name="email" class="form-control" id="admin-create-email" placeholder="Masukan email..">
        </div>
        <div class="mb-3">
          <label class="form-label">Role</label>
          <select name="role" id="admin-create-role" class="form-control" required>
            <option value="STAFF">STAFF</option>
            <option value="ADMIN">ADMIN</option>
            <option value="MANAGER">MANAGER</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="admin-create-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="admin-update-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Staff</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="admin-update-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="admin-update-name" placeholder="Masukan nama staff.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">PIN</label>
          <input type="number" name="pin" class="form-control" id="admin-update-pin" placeholder="Masukan PIN.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Telepon (Opsional)</label>
          <input type="text" name="phone" class="form-control" id="admin-update-phone" placeholder="Masukan nomor telepon..">
        </div>
        <div class="mb-3">
          <label class="form-label">Email (Opsional)</label>
          <input type="text" name="email" class="form-control" id="admin-update-email" placeholder="Masukan email..">
        </div>
        <div class="mb-3">
          <label class="form-label">Role</label>
          <select name="role" id="admin-update-role" class="form-control" required>
            <option value="STAFF">STAFF</option>
            <option value="ADMIN">ADMIN</option>
            <option value="MANAGER">MANAGER</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="admin-update-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="admin-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Staff</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Apakah kamu yakin menghapus staff tersebut?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="admin-delete-button" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>