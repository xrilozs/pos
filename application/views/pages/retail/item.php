    <!-- Body -->
    <section style='margin-top:50px;'>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Daftar Item</h1>
          </div>
          <div class="col-6 d-flex justify-content-end">
            <button 
              id="item-create-toggle"
              class="rounded-pill btn btn-success w-50" 
              data-bs-toggle="modal" data-bs-target="#item-create-modal"
            >
              <i class="fas fa-plus"></i> BUAT
            </button>
          </div>
        </div>
        
        <div
          class="row px-3 my-5 overflow-y-scroll"
          style="height: 70vh"
        >
          <div class="col">
            <table class="table" id="item-datatable">
              <thead>
                <tr>
                  <th scope="col">Gambar</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Stock Qty</th>
                  <th scope="col">Barcode</th>
                  <th scope="col">Harga</th>
                  <th scope="col">Kategori</th>
                  <th scope="col">Tanggal Expired</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

<div class="modal" tabindex="-1" id="item-create-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Item</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="item-create-form">
        <div class="mb-3">
          <label class="form-label">Gambar</label>
          <input type="file" name="image" class="form-control" id="item-create-image" accept="image/*" required>
        </div>
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="item-create-name" placeholder="Masukan nama.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Barcode</label>
          <input type="text" name="barcode" class="form-control" id="item-create-barcode" placeholder="Masukan barcode..">
        </div>
        <div class="mb-3">
          <label class="form-label">Kategori</label>
          <select name="category" id="item-create-category" class="form-control category-option" required>
            <option>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Harga</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">$</span>
            <input type="text" name="price" class="form-control price-field" id="item-create-price" placeholder="Contoh: 10.10" required>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Stock Qty</label>
          <input type="number" name="stock_qty" class="form-control" id="item-create-stock-qty" placeholder="Masukan Stock Qty.." value="100" required>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="item-create-description" placeholder="Masukan deskripsi.."></textarea>
        </div>
        <div class="mb-3">
          <label class="form-label">Expired Date</label>
          <div class="input-group date" id="expired-datepicker">
            <input type="text" name="expired_date" class="form-control" id="item-create-expired-date" placeholder="Date" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="item-create-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="item-update-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kategori Item</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="item-update-form">
        <div id="item-update-image-section"></div>
        <div class="mb-3">
          <label class="form-label">Gambar</label>
          <input type="file" name="image" class="form-control" id="item-update-image" accept="image/*">
        </div>
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="item-update-name" placeholder="Masukan nama.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Barcode</label>
          <input type="text" name="barcode" class="form-control" id="item-update-barcode" placeholder="Masukan barcode..">
        </div>
        <div class="mb-3">
          <label class="form-label">Kategori</label>
          <select name="category" id="item-update-category" class="form-control category-option" required>
            <option>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Harga</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">$</span>
            <input type="text" name="price" class="form-control price-field" id="item-update-price" placeholder="Contoh: 10.10" required>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Stock Qty</label>
          <input type="number" name="stock_qty" class="form-control" id="item-update-stock-qty" placeholder="Masukan Stock Qty.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="item-update-description" placeholder="Masukan deskripsi.."></textarea>
        </div>
        <div class="mb-3">
          <label class="form-label">Expired Date</label>
          <div class="input-group date" id="expired-datepicker">
            <input type="text" name="expired_date" class="form-control" id="item-update-expired-date" placeholder="Date" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="item-update-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="item-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Item</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Apakah kamu yakin menghapus item tersebut?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="item-delete-button" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>