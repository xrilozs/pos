    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Daftar Diskon</h1>
          </div>
          <div class="col-6 d-flex justify-content-end">
            <button 
              id="discount-create-toggle"
              class="rounded-pill btn btn-success w-50" 
              data-bs-toggle="modal" data-bs-target="#discount-create-modal"
            >
              <i class="fas fa-plus"></i> BUAT
            </button>
          </div>
        </div>
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
            <table class="table" id="discount-datatable">
              <thead>
                <tr>
                  <th scope="col">Nama</th>
                  <th scope="col">Kode</th>
                  <th scope="col">Tipe</th>
                  <th scope="col">Nilai</th>
                  <th scope="col">Periode Mulai</th>
                  <th scope="col">Periode Akhir</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

<div class="modal" tabindex="-1" id="discount-create-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Diskon</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="discount-create-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="discount-create-name" placeholder="Masukan nama diskon.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Kode</label>
          <input type="text" name="code" class="form-control" id="discount-create-code" placeholder="Masukan kode diskon.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Tipe</label>
          <select name="type" id="discount-create-type" class="form-control" required>
            <option value="AMOUNT">Nominal</option>
            <option value="PERCENTAGE">Persentase</option>
          </select>
        </div>
        <div class="mb-3" id="nominal-create">
          <label class="form-label">Nominal</label>
          <div class="input-group">
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                $
              </span>
            </span>
            <input type="number" name="value" class="form-control" id="discount-create-nominal" placeholder="Masukan nilai diskon.." required>
          </div>
        </div>
        <div class="mb-3" id="percentage-create" style="display:none;">
          <label class="form-label">Persentase</label>
          <div class="input-group">
            <input type="number" name="value" class="form-control" id="discount-create-percentage" placeholder="Masukan nilai diskon.." required>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                %
              </span>
            </span>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Tanggal Mulai</label>
          <div class="input-group date" id="start-datepicker-create">
            <input type="text" name="periode_start" class="form-control" id="discount-create-start-date" data-date-format="yyyy-mm-dd" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Tanggal Akhir</label>
          <div class="input-group date" id="end-datepicker-create">
            <input type="text" name="periode_end" class="form-control" id="discount-create-end-date"  data-date-format="yyyy-mm-dd" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="discount-create-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="discount-update-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kategori Item</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="discount-update-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="discount-update-name" placeholder="Masukan nama kategori.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Kode</label>
          <input type="text" name="code" class="form-control" id="discount-update-code" placeholder="Masukan kode diskon.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Tipe</label>
          <select name="type" id="discount-update-type" class="form-control" required>
            <option value="AMOUNT">Nominal</option>
            <option value="PERCENTAGE">Persentase</option>
          </select>
        </div>
        <div class="mb-3" id="nominal-update">
          <label class="form-label">Nominal</label>
          <div class="input-group">
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                $
              </span>
            </span>
            <input type="number" name="value" class="form-control" id="discount-update-nominal" placeholder="Masukan nilai diskon.." required>
          </div>
        </div>
        <div class="mb-3" id="percentage-update" style="display:none;">
          <label class="form-label">Persentase</label>
          <div class="input-group">
            <input type="number" name="value" class="form-control" id="discount-update-percentage" placeholder="Masukan nilai diskon.." required>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                %
              </span>
            </span>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Tanggal Mulai</label>
          <div class="input-group date" id="start-datepicker-update">
            <input type="text" name="periode_start" class="form-control" id="discount-update-start-date" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
        <div class="mb-3">
          <label class="form-label">Tanggal Akhir</label>
          <div class="input-group date" id="end-datepicker-update">
            <input type="text" name="periode_end" class="form-control" id="discount-update-end-date" required/>
            <span class="input-group-append">
              <span class="input-group-text bg-light d-block">
                <i class="fa fa-calendar"></i>
              </span>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="discount-update-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="discount-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Diskon</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Apakah kamu yakin menghapus diskon tersebut?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="discount-delete-button" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>