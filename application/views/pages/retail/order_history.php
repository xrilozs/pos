    <!-- Body -->
    <section style='margin-top:50px;'>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Order History</h1>
          </div>
        </div>
        <div class="row pt-3">
          <!-- <div class="col-4">
            <form class="d-flex" role="search">
              <input
                class="form-control rounded-pill border border-secondary"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
            </form>
          </div> -->
          <div class="col">
            <div class="input-group date" id="history-datepicker">
              <input type="text" name="history_date" class="form-control" id="history-date" placeholder="Date" required/>
              <span class="input-group-append">
                <span class="input-group-text bg-light d-block">
                  <i class="fa fa-calendar"></i>
                </span>
              </span>
            </div>
          </div>
          <div class="col">
            <select name="payment_type" id="history-payment-type" class="form-control">
              <option value="">Semua Payment</option>
              <option value="CASH">Cash</option>
              <option value="CREDIT CARD">Credit Card</option>
            </select>
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-success w-100" id="history-button">SHOW</button>
          </div>
        </div>
        <div
          class="row px-3 my-5 overflow-y-scroll"
          style="height: 70vh"
          id="history-section"
        >
          <div class="col">
            <!-- <div class="mb-4">
              <b>Number of Order:</b> <span id="history-order-total">-</span>
              <b>Total:</b> <span id="history-sales-total">-</span>
            </div> -->
            <table class="table" id="history-datatable">
              <thead>
                <tr>
                  <th scope="col">#Invoice</th>
                  <th scope="col">Date</th>
                  <th scope="col">Payment Method</th>
                  <th scope="col">Total</th>
                  <th scope="col">Items</th>
                  <th scope="col">Timestamp</th>
                  <th scope="col">Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>             
            </table>
          </div>
        </div>
      </div>
    </section>

    <div class="modal" id="history-refund-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Refund Order</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <p>Apakah kamu yakin ingin refund order tersebut?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
            <button type="button" id="history-refund-button" class="btn btn-warning">Iya</button>
          </div>
        </div>
      </div>
    </div>