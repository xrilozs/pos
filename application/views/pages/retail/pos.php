    <!-- Body -->
    <section style='margin-top:70px;'>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 col-md-7">
            <div class="row">
              <div class="col-lg-10 col-md-9 pr-0">
                <form class="d-flex bg-secondary-subtle py-2" role="search">
                  <!-- <div>
                    <p class="small">Order # <span class="h4">53</span></p>
                  </div> -->
                  <!-- <select id="pos-menu-search" class="form-control me-2"></select> -->
                  <input type="text" id="menu-search" placeholder="cari item.." class="form-control me-2">
                </form>
                <div class="pt-4 overflow-y-scroll" style="height: 50vh">
                  <table class="table" style="width:100%">
                    <tbody id="pos-selected-menu">
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-lg-2 col-md-3 text-center bg p-0">
                <div class="menu" id="pos-category">
                </div>
                <div></div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 bg-light fixed-bottom">
                <table class="table">
                  <tbody>
                    <tr>
                      <th scope="col">DISCOUNT</th>
                      <td scope="col" id="item-discount">-</td>
                      <th scope="col" class="text-end h4">Total</th>
                    </tr>
                    <tr>
                      <th scope="col">SUBTOTAL</th>
                      <td scope="col" id="item-subtotal">-</td>
                      <td scope="col" class="text-end"><span id="item-total">0</span> Items</td>
                    </tr>
                    <tr>
                      <th scope="col">TAX</th>
                      <td scope="col">-</td>
                      <!-- <td scope="col">10%</td> -->
                      <td scope="col" class="text-success h3 fw-bold text-end" id="item-grandtotal">-</td>
                    </tr>
                    <tr>
                      <td>
                        <button id="clear-items-toggle" class="btn btn-outline-dark w-100" data-bs-toggle="modal" data-bs-target="#clear-items-model" disabled>
                          CLEAR
                        </button>
                      </td>
                      <td>
                        <button class="btn btn-outline-dark w-100" id="discount-toggle" data-bs-toggle="modal" data-bs-target="#discount-model">
                          DISCOUNT
                        </button>
                      </td>
                      <td>
                        <div class="btn-group w-100">
                          <button type="button" class="btn btn-success btn-block dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            PAY
                          </button>
                          <ul class="dropdown-menu">
                            <li>
                              <button type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#cash-modal">
                                Cash
                              </button>
                            </li>
                            <li>
                              <button type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#credit-card-modal">
                                Credit Card
                              </button>
                            </li>
                          </ul>
                        </div>
                        <!-- <button class="btn btn-success w-100">PAY</button> -->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-5 pt-3 overflow-scroll" style="height: 95vh">
            <div class="row menu-harga">
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="clear-items-model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Clear Items</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah Anda yakin ingin membatalkan semua input yang sudah dilakukan?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
            <button type="button" class="btn btn-primary" id="clear-items-button">Iya</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="clear-item-model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Clear Item</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah Anda yakin ingin membatalkan item yang Anda pilih?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
            <button type="button" class="btn btn-primary" id="clear-item-button">Iya</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="edit-item-model" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Diskon</h5>
            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
          </div>
          <div class="modal-body">
            <form id="edit-item-form">
            <div class="mb-3">
              <label class="form-label">Item</label>
              <input type="text" class="form-control" id="edit-item-name" required>
            </div>
            <div class="mb-3">
              <label class="form-label">Qty</label>
              <input type="number" class="form-control" id="edit-item-qty" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="discount-button">Apply</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="discount-model" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Diskon</h5>
            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
          </div>
          <div class="modal-body">
            <form id="discount-form">
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Diskon</label>
              <select id="discount-search" class="form-control me-2"></select>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="discount-button">Apply</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="credit-card-modal" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Bayar menggunakan credit card</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="credit-card-form">
            Apakah pembayaran sudah berhasil dilakukan?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" id="credit-card-button">
              <i class="fas fa-print"></i> Bayar
            </button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="cash-modal" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Bayar menggunakan cash</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="cash-form">
            <div class="form-group">
              <label>Total</label>
              <input type="text" class="form-control" name="total" id="cash-total" disabled>
            </div>
            <div class="form-group">
              <label>Jumlah Uang yang dibayarkan</label>
              <input type="number" class="form-control" name="total_pay" id="cash-total-pay" required>
            </div>
            <div class="form-group">
              <label>Kembalian</label>
              <input type="text" class="form-control" name="exchange	" id="cash-exchange" disabled>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" id="cash-button">
              <i class="fas fa-print"></i> Bayar
            </button>
            </form>
          </div>
        </div>
      </div>
    </div>