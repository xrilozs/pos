    <!-- Body -->
    <section style='margin-top:50px;'>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Report</h1>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col">
            <button class="rounded-pill btn btn-outline-secondary w-100">
              PRINT
            </button>
          </div>
          <div class="col">
           
          </div>
          <div class="col">
            <div class="input-group date" id="start-datepicker">
              <input type="text" name="start_date" class="form-control" id="report-start-date" placeholder="start date" required/>
              <span class="input-group-append">
                <span class="input-group-text bg-light d-block">
                  <i class="fa fa-calendar"></i>
                </span>
              </span>
            </div>
          </div>
          <div class="col">
            <div class="input-group date" id="end-datepicker">
              <input type="text" name="end_date" class="form-control" id="report-end-date" placeholder="end date" required/>
              <span class="input-group-append">
                <span class="input-group-text bg-light d-block">
                  <i class="fa fa-calendar"></i>
                </span>
              </span>
            </div>
          </div>
          <div class="col">
            <button id="report-button" class="rounded-pill btn btn-success w-100" disabled>SHOW</button>
          </div>
        </div>
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; display:none;"
          id="report-section"
        >
          <div class="col">
              
              <table class="table">
              <thead>
                <tr>
                  <th scope="row" id="report-date">Date 20/05/23 - 22/05/23</th>
                <tr>
                <tr>
                  <th scope="col">Total Sales</th>
                  <th scope="col">Number of Sales</th>
                  <th scope="col">Discounts Total</th>
                  <th scope="col">Tax</th>
                  <th scope="col">Cash Payments</th>
                  <th scope="col">Card Payments</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row" id="report-total-sales">-</th>
                  <td id="report-total-order">-</td>
                  <td id="report-total-discount">-</td>
                  <td id="report-total-tax">-</td>
                  <td id="report-total-cash">-</td>
                  <td id="report-total-cc">-</td>
                </tr>
              </tbody>
            </table>
              
          </div>
        </div>
      </div>
    </section>
