    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Daftar Menu</h1>
          </div>
          <div class="col-6 d-flex justify-content-end">
            <button 
              class="rounded-pill btn btn-success w-50" 
              data-bs-toggle="modal" data-bs-target="#menu-create-modal"
            >
              <i class="fas fa-plus"></i> BUAT
            </button>
          </div>
        </div>
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
            <table class="table" id="menu-datatable">
              <thead>
                <tr>
                  <th scope="col">Gambar</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Tipe</th>
                  <th scope="col">Harga</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

<div class="modal" tabindex="-1" id="menu-create-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="menu-create-form">
        <div class="mb-3">
          <label class="form-label">Gambar</label>
          <input type="file" name="image" class="form-control" id="menu-create-image" accept="image/*" required>
        </div>
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="menu-create-name" placeholder="Masukan nama.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Kategori</label>
          <select name="category" id="menu-create-category" class="form-control category-option" required>
            <option>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Harga</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">$</span>
            <input type="text" name="price" class="form-control price-field" id="menu-create-price" placeholder="Contoh: 10.10" required>
          </div>
          <!-- <input type="number" name="price" class="form-control" id="menu-create-price" placeholder="Masukan harga.." required> -->
        </div>
        <div class="mb-3">
          <label class="form-label">Tipe</label>
          <select name="type" id="menu-create-type" class="form-control" required>
            <option value="KITCHEN">Kitchen</option>
            <option value="BAR">Bar</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="menu-create-description" placeholder="Masukan deskripsi.."></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="menu-create-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="menu-update-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kategori Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="menu-update-form">
        <div id="menu-update-image-section"></div>
        <div class="mb-3">
          <label class="form-label">Gambar</label>
          <input type="file" name="image" class="form-control" id="menu-update-image" accept="image/*">
        </div>
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="menu-update-name" placeholder="Masukan nama.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Kategori</label>
          <select name="category" id="menu-update-category" class="form-control category-option" required>
            <option>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Harga</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">$</span>
            <input type="text" name="price" class="form-control price-field" id="menu-update-price" placeholder="Contoh: 10.10" required>
          </div>
          <!-- <input type="number" name="price" class="form-control" id="menu-update-price" placeholder="Masukan harga.." required> -->
        </div>
        <div class="mb-3">
          <label class="form-label">Tipe</label>
          <select name="type" id="menu-update-type" class="form-control" required>
            <option value="KITCHEN">Kitchen</option>
            <option value="BAR">Bar</option>
          </select>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="menu-update-description" placeholder="Masukan deskripsi.."></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="menu-update-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="menu-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Apakah kamu yakin menghapus menu tersebut?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="menu-delete-button" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>