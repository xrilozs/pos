    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Report</h1>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col">
            <button class="rounded-pill btn btn-outline-secondary w-100">
              PRINT
            </button>
          </div>
          <div class="col">
           
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-outline-secondary w-100">
              START DATE
            </button>
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-outline-secondary w-100">
              END DATE
            </button>
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-success w-100">SHOW</button>
          </div>
        </div>
        
        
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
              
              <table class="table">
              <thead>
                                  <tr>
                  <th scope="row">Date 20/05/23 - 22/05/23</th>
                <tr>
                <tr>
                  <th scope="col">Total Sales</th>
                  <th scope="col">Number of Sales</th>
                  <th scope="col">Discounts Total</th>
                  <th scope="col">Tax</th>
                  <th scope="col">Cash Payments</th>
                  <th scope="col">Card Payments</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">$10.240,50</th>
                  <td>240</td>
                  <td>-$420.006</td>
                  <td>$133.57</td>
                  <td>$5123.20</td>
                  <td>$4712.50</td>
                </tr>
              </tbody>
            </table>
              
          </div>
        </div>
      </div>
    </section>
