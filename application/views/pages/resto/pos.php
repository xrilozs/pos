    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-10">
                <form class="d-flex bg-secondary-subtle py-2" role="search">
                  <div>
                    <p class="small">Order # <span class="h4">53</span></p>
                  </div>
                  <!-- <input
                    class="form-control me-2"
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                  /> -->
                  <select id="pos-menu-search" class="form-control me-2"></select>
                  <button class="btn btn-outline-success" type="submit">
                    Table
                  </button>
                </form>
                <div class="pt-4 overflow-y-scroll" style="height: 50vh">
                  <table class="table" style="width:100%">
                    <tbody id="pos-selected-menu">
                       
                      <!-- <tr>
                        <td style="width:40%;">W3. Mie Goreng</td>
                        <td style="text-align: right;width:10%;">2</td>
                        <td style="text-align: right;width:20%;">$17.95</td> 
                        <td style="width:30%;"><button class="btn btn-secondary">edit</button>
                        <button class="btn btn-danger">X</button></td>
                      </tr>
                      <tr>
                        <td style="width:40%">W1. Nasi Goreng</td>
                        <td style="text-align: right;width:10%;">2</td>
                        <td style="text-align: right;width:20%;">$17.95</td> 
                        <td style="width:30%;"><button class="btn btn-secondary">edit</button>
                        <button class="btn btn-danger">X</button></td>
                      </tr>
                      <tr>
                        <td style="width:40%">Ice Tea</td>
                        <td style="text-align: right;width:10%;">2</td>
                        <td style="text-align: right;width:20%;">$5.00</td> 
                        <td style="width:30%;"><button class="btn btn-secondary">edit</button>
                        <button class="btn btn-danger">X</button></td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-2 text-center bg p-0">
                <div class="menu" id="pos-category">
                </div>
                <div></div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 bg-light fixed-bottom">
                <table class="table">
                  <tbody>
                    <tr>
                      <th scope="col">DISCOUNT</th>
                      <td scope="col">-</td>
                      <th scope="col" class="text-end h4">Total</th>
                    </tr>
                    <tr>
                      <th scope="col">SUBTOTAL</th>
                      <td scope="col">-</td>
                      <td scope="col" class="text-end">0 Items</td>
                    </tr>
                    <tr>
                      <th scope="col">TAX</th>
                      <td scope="col">10%</td>
                      <td scope="col" class="text-success h3 fw-bold text-end">-</td>
                    </tr>
                    <tr>
                      <td>
                        <button class="btn btn-outline-dark w-100">
                          CLEAR
                        </button>
                      </td>
                      <td>
                        <button class="btn btn-outline-dark w-100">
                          SEND
                        </button>
                      </td>
                      <td>
                        <button class="btn btn-outline-dark w-100">
                          DISCOUNT
                        </button>
                      </td>
                      <td>
                        <button class="btn btn-success w-100">PAY</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-6 pt-3 overflow-scroll" style="height: 95vh">
            <div class="row menu-harga">
            </div>
          </div>
        </div>
      </div>
    </section>
