    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Held Order</h1>
          </div>
        </div>
        <div class="row pt-3">
          <div class="col-4">
            <form class="d-flex" role="search">
              <input
                class="form-control rounded-pill border border-secondary"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
            </form>
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-outline-secondary w-100">
              ORDER TYPE
            </button>
          </div>
          <div class="col">
            
          </div>
          <div class="col">
           
          </div>
          <div class="col">
            <button class="rounded-pill btn btn-success w-100">SHOW</button>
          </div>
        </div>
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
              <div class="col overflow-scroll" style="height: 95vh">
            <div class="row menu-harga">
              <div class="col">
                  <div class="card bg-light mb-3" style="max-width: 18rem;">
                      <div class="card-header text-white bg-primary mb-3">
                        <h5>Table 3</h5>
                      </div>
                      <div class="card-body">
                        <table class="table" style="width:100%">
                            <tr>
                            <td style="width:50%;">Order</td>
                            <td style="text-align:right;width:50%">#5</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Time</td>
                            <td style="text-align:right;width:50%">12.20am</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Cost</td>
                            <td style="text-align:right;width:50%">$40.20</td>
                            </tr>
                        </table>
                      </div>
                    </div>
              </div>
              <div class="col">
                  <div class="card bg-light mb-3" style="max-width: 18rem;">
                      <div class="card-header text-white bg-primary mb-3">
                        <h5>Table 6</h5>
                      </div>
                      <div class="card-body">
                        <table class="table" style="width:100%">
                            <tr>
                            <td style="width:50%">Order</td>
                            <td style="text-align:right;width:50%">#6</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Time</td>
                            <td style="text-align:right;width:50%">12.40am</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Cost</td>
                            <td style="text-align:right;width:50%">$16.50</td>
                            </tr>
                        </table>
                      </div>
                    </div>
              </div>
              <div class="col">
                  <div class="card bg-light mb-3" style="max-width: 18rem;">
                      <div class="card-header text-white bg-primary mb-3">
                        <h5>Adrian</h5>
                      </div>
                      <div class="card-body">
                        <table class="table" style="width:100%">
                            <tr>
                            <td style="width:50%">Order</td>
                            <td style="text-align:right;width:50%">#7</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Time</td>
                            <td style="text-align:right;width:50%">12.50am</td>
                            </tr>
                            <tr>
                            <td style="width:50%">Cost</td>
                            <td style="text-align:right;width:50%">$10.00</td>
                            </tr>
                        </table>
                      </div>
                    </div>
              </div>
              
              <div class="col">
               
              </div>
              <div class="col">
               
              </div>
              <div class="col">

              </div>
            </div>
          </div>
            
          </div>
        </div>
      </div>
    </section>
