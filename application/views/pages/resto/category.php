    <!-- Body -->
    <section>
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-6">
            <h1>Daftar Kategori Menu</h1>
          </div>
          <div class="col-6 d-flex justify-content-end">
            <button 
              class="rounded-pill btn btn-success w-50" 
              data-bs-toggle="modal" data-bs-target="#category-create-modal"
            >
              <i class="fas fa-plus"></i> BUAT
            </button>
          </div>
        </div>
        
        <div
          class="row px-3 mt-5"
          style="height: 85vh !important; overflow-y: scroll"
        >
          <div class="col">
            <table class="table" id="category-datatable">
              <thead>
                <tr>
                  <th scope="col">Nama</th>
                  <th scope="col">Deskripsi</th>
                  <th scope="col">Urutan</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

<div class="modal" tabindex="-1" id="category-create-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kategori Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="category-create-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="category-create-name" placeholder="Masukan nama kategori.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="category-create-description" placeholder="Masukan Deskripsi.."></textarea>
        </div>
        <div class="mb-3">
          <label class="form-label">Urutan</label>
          <input type="number" name="sort_no" class="form-control" id="category-create-sort" placeholder="Masukan urutan.." required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="category-create-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="category-update-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kategori Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="category-update-form">
        <div class="mb-3">
          <label class="form-label">Nama</label>
          <input type="text" name="name" class="form-control" id="category-update-name" placeholder="Masukan nama kategori.." required>
        </div>
        <div class="mb-3">
          <label class="form-label">Deskripsi</label>
          <textarea name="description" class="form-control" id="category-update-description" placeholder="Masukan Deskripsi.."></textarea>
        </div>
        <div class="mb-3">
          <label class="form-label">Urutan</label>
          <input type="number" name="sort_no" class="form-control" id="category-update-sort" placeholder="Masukan urutan.." required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="category-update-button" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="category-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Kategori Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Apakah kamu yakin menghapus kategori tersebut?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" id="category-delete-button" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>