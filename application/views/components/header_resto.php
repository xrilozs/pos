<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Sendok Garpu</title>
    <!-- Bootstrap 5 -->
    <link href="<?=base_url('assets/vendor/bootstrap.min.css');?>" rel="stylesheet" /> 
    <!-- Select2 4 -->
    <link href="<?=base_url('assets/vendor/select2.min.css');?>" rel="stylesheet" />
    <!-- Fontawesome 5 -->
    <link href="<?=base_url('assets/vendor/fontawesome.min.css');?>" rel="stylesheet">
    <!-- Datatable 1.12 -->
    <link href="<?=base_url('assets/vendor/jquery.dataTables.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css');?>" />
  </head>
  <body>
    <nav class="navbar navbar-expand navbar-dark" style="background-color:#6c221f;">
      <div class="container-fluid">
        <a class="navbar-brand" href="<?=base_url('resto/point-of-sales');?>">Sendok Garpu</a>
        <div class="">
          <button class="btn btn-light" type="button" id="" data-bs-toggle="" aria-expanded="false">
            <a class="dropdown-item" href="<?=base_url("retail/point-of-sales");?>">Grocery</a>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav mx-auto" style="padding-left: 15%">
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle text-white"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Point Of Sale
              </a>
              <ul class="dropdown-menu bg-secondary">
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/point-of-sales');?>">Point Of Sale</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/held-order');?>">Held Orders</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/order-history');?>">Order History</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/report');?>">Report</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/discount');?>">Discount</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/category');?>">Category</a>
                </li>
                <li>
                  <a class="dropdown-item text-white" href="<?=base_url('resto/menu');?>">Menu</a>
                </li>
                <li>
                  <a class="dropdown-item text-white non-staff-menu" href="<?=base_url('resto/admin');?>">Staff</a>
                </li>
              </ul>
            </li>
          </ul>
          <ul class="navbar-nav ms-auto">
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle text-white account-name"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Welcome
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" id="logout-button" href="#">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>