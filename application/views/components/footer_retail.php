
    <!-- JQuery 3 -->
    <script src="<?=base_url('assets/vendor/jquery-3.6.1.min.js');?>"></script>
    <!-- JQuery Validation 1.1 -->
    <script src="<?=base_url('assets/vendor/jquery.validate.min.js');?>"></script>
    <!-- Fontawesome 5 -->
    <script src="<?=base_url('assets/vendor/fontawesome.min.js');?>"></script>
    <!-- Sweetalert2 11 -->
    <script src="<?=base_url('assets/vendor/sweetalert2@11.js');?>"></script>
    <!-- Datatable 1.1 -->
    <script src="<?=base_url('assets/vendor/jquery.dataTables.min.js');?>"></script>
    <!-- Select2 4 -->
    <script src="<?=base_url('assets/vendor/select2.min.js');?>"></script>
    <!-- Bootstrap 5 -->
    <script src="<?=base_url('assets/vendor/bootstrap.bundle.min.js');?>"></script>
    <!-- Bootstrap Date Picker -->
    <script src="<?=base_url('assets/vendor/bootstrap-datepicker.min.js');?>"></script>
    <!-- Custom CSS -->
    <script>
      const WEB_URL = '<?=BASE_URL;?>';
      const API_URL = '<?=API_URL;?>';
      const PAGE = '<?=$page;?>';
    </script>
    <script src="<?=base_url('assets/js/common.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php if ($page=="pos"): ?>
      <script src="<?=base_url('assets/js/retail_pos.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="order history"): ?>
      <script src="<?=base_url('assets/js/retail_history.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="report"): ?>
      <script src="<?=base_url('assets/js/retail_report.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="admin"): ?>
      <script src="<?=base_url('assets/js/retail_admin.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="item"): ?>
      <script src="<?=base_url('assets/js/retail_item.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="category"): ?>
      <script src="<?=base_url('assets/js/retail_category.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php elseif ($page=="discount"): ?>
      <script src="<?=base_url('assets/js/retail_discount.js')."?v=".date(ASSET_VERSION);?>"></script>
    <?php endif; ?>
  </body>
</html>