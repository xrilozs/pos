<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Snap</title>
</head>
<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?=MIDTRANS_SNAP;?>/snap.js" data-client-key="<?=MIDTRANS_CLIENTKEY;?>"></script>
    <script>
        snap.pay("<?=$token;?>")
    </script>
</body>
</html>