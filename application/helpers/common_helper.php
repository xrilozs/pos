<?php
  function check_parameter($params){
    foreach($params as $param){
      if(is_null($param)){
        return false;
      }
    }
    return true;
  }

  function check_parameter_by_keys($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return false;
      }
    }
    return true;
  }

  function get_uniq_id(){
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
      mt_rand( 0, 0xffff ),
      mt_rand( 0, 0x0C2f ) | 0x4000,
      mt_rand( 0, 0x3fff ) | 0x8000,
      mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
    );
  }

  function set_output($resp){
    $CI =& get_instance();
    $CI->output
        ->set_header('Cache-Control: max-age=3600') // Cache selama 1 jam (3600 detik)
        ->set_header('Access-Control-Allow-Origin: *')
        ->set_status_header($resp['code'])
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($resp));
  }

  function logging($type, $message, $data=null){
    log_message($type, $message);
    if($data){
      log_message($type, 'Request/Response: '.json_encode($data));
    }
  }
?>
