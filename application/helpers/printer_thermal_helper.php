<?php
  // require_once APPPATH . '../vendor/autoload.php';
  require_once('./vendor/autoload.php');
  use Mike42\Escpos\Printer;
  use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
  use Mike42\Escpos\EscposImage;
  use Mike42\Escpos\PrintConnectors\RawbtPrintConnector;
  use Mike42\Escpos\CapabilityProfile;

  function print_invoice($invoice_number, $items, $total, $diskon, $subtotal, $cash, $change){
    try {
      // Enter the share name for your USB printer here
      // $connector = null;
      $profile = CapabilityProfile::load("POS-5890");
      $connector = new RawbtPrintConnector();
      $printer = new Printer($connector, $profile);
      // $connector = new WindowsPrintConnector("POS58 Printer");
      // $printer = new Printer($connector);
      #Information
      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer -> setTextSize(2, 4);
      $printer -> text("Sendok Garpu\n");
      $printer -> text("Indonesian\n");
      $printer -> text("Grocery Shop\n");
      $printer -> feed();
      $printer -> selectPrintMode(Printer::MODE_FONT_A);
      $printer -> text("Shop 6/97 Elizabeth Street\n");
      $printer -> text("Brisbane City, Queensland 4000\n");
      $printer -> feed();
      $printer -> text("ABN 42 167 986 772\n");
      $printer -> text("Phone : (07) 4602 9979\n");
      $printer -> text("www.sendokgarpu.com.au\n");
      $printer -> feed(2);
      #Total
      $printer -> text("Total\n");
      $printer -> text(str_repeat("=", 30) . "\n");
      $printer -> setTextSize(2, 4);
      $printer -> text("AUD$total\n");
      $printer -> selectPrintMode(Printer::MODE_FONT_A);
      $printer -> text(str_repeat("=", 30) . "\n");
      $printer -> feed(2);

      #Items Header
      $printer -> setJustification(Printer::JUSTIFY_LEFT);
      $printer -> selectPrintMode(Printer::MODE_FONT_B);      
      $printer->text("Items");
      $printer->text(str_repeat(" ", 20));
      $printer->text("Price");
      $printer->text("\n");

      $printer -> text(str_repeat("-", 30) . "\n");
      // $groceryList = [
      //   array("name" => "Item #1", "price" => "AUD10.00"),
      //   array("name" => "Barang #2", "price" => "AUD11.00"),
      //   array("name" => "Goods or stuff #1", "price" => "AUD12.00"),
      // ];

      //Items list
      foreach ($items as $row) {
        $textItem = $row['name'];
        $textPrice = "AUD".$row['price'];
        $textItemSize = strlen($textItem);
        $textPriceSize = strlen($textPrice);

        if($textItemSize > 15){
          $chunks = str_split($textItem, 15);
          $textItem = implode("\n", $chunks);
          $textItemSize = strlen($chunks[sizeof($chunks)-1]);
        }
        $blankSize = 30 - ($textItemSize + $textPriceSize);
        $printer -> text($textItem);
        $printer -> text(str_repeat(" ", $blankSize));
        $printer -> text("$textPrice");
        $printer->text("\n");
      }

      $printer -> text(str_repeat(".", 30));
      $printer -> feed();

      #Subtotal
      $subtotal = "AUD$subtotal";
      $blankSize = 30-8-strlen($subtotal);
      $printer->text("Subtotal");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($subtotal);
      $printer -> feed();
      #Diskon
      $diskon = "AUD$diskon";
      $blankSize = 30-6-strlen($diskon);
      $printer->text("Diskon");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($diskon);
      $printer -> feed();
      #Total
      $total = "AUD$total";
      $blankSize = 30-5-strlen($total);
      $printer->text("Total");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($total);
      $printer -> feed();

      $printer -> text(str_repeat(".", 30));
      $printer -> feed();

      $printer -> selectPrintMode(Printer::MODE_FONT_A);      
      #Cash
      $cash = "AUD$cash";
      $blankSize = 30-4-strlen($cash);
      $printer->text("Cash");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($cash);
      $printer -> feed();
      #Cash
      $change = "AUD$change";
      $blankSize = 30-10-strlen($change);
      $printer->text("Change due");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($change);
      $printer -> feed();

      $printer -> text(str_repeat("-", 30));
      $printer -> feed();

      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer->text(date('M d, Y h:i A')."\n");
      $printer->text("Staff at Register: Grocery SG\n");
      $printer->text("Receipt: $invoice_number");
      $printer -> feed();

      $printer -> cut();      
      $printer -> close();
    } catch (Exception $e) {
      $err_msg = "Couldn't print to this printer: " . $e -> getMessage() . "\n";
      logging('error', $err_msg);
    }
  }
?>