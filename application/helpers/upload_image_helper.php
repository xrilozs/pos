<?php
  function upload_image($file, $destination, $is_create_thumbnail=false) {
    $ok_ext = array(
      'jpg',
      'png',
      'jpeg',
      'bmp',
      'ico'
    );

    $filename       = explode(".", $file["name"]);
    $file_extension = $filename[count($filename) - 1];
    $file_weight    = $file['size'];
    $file_type      = $file['type'];
    if($file['error']){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload image failed!'
      );
      return $data;
    }
    if (!in_array(strtolower($file_extension), $ok_ext)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'File with this type is not allowed!'
      );
      return $data;
    }
    if($file_weight > 2097152){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Max file size is 2MB!'
      );
      return $data;
    }
    $file_new_name = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
    if (!move_uploaded_file($file['tmp_name'], $destination . $file_new_name)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload image failed!'
      );
      return $data;
    }
    $origin_img = $destination . $file_new_name;
    $thumbnail_url = null;
    if($is_create_thumbnail){
      $resize_img = resize_image($destination, $file_new_name);
      if($resize_img['status'] == 'failed'){
        $data = array(
          'status' 	=> 	'failed',
          'message'	=>	'Upload image failed! compressing failed.'
        );
        return $data;
      }
      $thumbnail_url = $resize_img['data'];
    }

    $data = array(
      'status' => 'success',
      'data' => array(
        'img_url' => $origin_img,
        'thumbnail_url' => $thumbnail_url
      )
    );
    return $data;
  }

  function remove_image($file_path){
    if (file_exists($file_path)) {
        if (unlink($file_path)) {
          return array(
            "success" => true,
            "message" => "Image file deleted successfully."
          );
        } else {
          return array(
            "success" => false,
            "message" => "Unable to delete the image file."
          );
        }
    } else {
      return array(
        "success" => false,
        "message" => "Unable to delete the image file."
      );
    }
  }
?>