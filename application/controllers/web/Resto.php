<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resto extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  public function point_of_sales(){
		$data['title']	= "Point of Sales";
		$data['page']   = "pos";

		$this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/pos', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function held_order(){
		$data['title']	= "Held Order";
		$data['page']   = "held order";

		$this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/held_order', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function order_history(){
		$data['title']	= "Order History";
		$data['page']   = "order history";

		$this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/order_history', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function report(){
		$data['title']	= "Report";
		$data['page']   = "report";

		$this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/report', $data);
		$this->load->view('components/footer_resto', $data);
  }

	public function menu(){
		$data['title']  = "Menu";
		$data['page']   = "menu";

    $this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/menu', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function category(){
		$data['title']	= "Menu Category";
		$data['page']   = "category";

    $this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/category', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function discount(){
		$data['title']	= "Discount";
		$data['page']   = "discount";

    $this->load->view('components/header_resto', $data);
		$this->load->view('pages/resto/discount', $data);
		$this->load->view('components/footer_resto', $data);
  }

  public function admin(){
		$data['title']        = "Admin";
		$data['page']         = "admin";

		$this->load->view('components/header_resto', $data);
		$this->load->view('pages/admin', $data);
		$this->load->view('components/footer_resto', $data);
  }
}