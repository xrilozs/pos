<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retail extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  public function point_of_sales(){
		$data['title']        = "Point of Sales";
		$data['page']         = "pos";

		$this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/pos', $data);
		$this->load->view('components/footer_retail', $data);
  }

  public function order_history(){
		$data['title']        = "Order History";
		$data['page']         = "order history";

		$this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/order_history', $data);
		$this->load->view('components/footer_retail', $data);
  }

  public function report(){
		$data['title']        = "Report";
		$data['page']         = "report";

		$this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/report', $data);
		$this->load->view('components/footer_retail', $data);
  }

	public function item(){
		$data['title']  = "Item";
		$data['page']   = "item";

    $this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/item', $data);
		$this->load->view('components/footer_retail', $data);
  }

  public function category(){
		$data['title']        = "Item Category";
		$data['page']         = "category";

    $this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/category', $data);
		$this->load->view('components/footer_retail', $data);
  }

  public function discount(){
		$data['title']        = "Discount";
		$data['page']         = "discount";

    $this->load->view('components/header_retail', $data);
		$this->load->view('pages/retail/discount', $data);
		$this->load->view('components/footer_retail', $data);
  }

	public function admin(){
		$data['title']        = "Admin";
		$data['page']         = "admin";

		$this->load->view('components/header_retail', $data);
		$this->load->view('pages/admin', $data);
		$this->load->view('components/footer_retail', $data);
  }
}