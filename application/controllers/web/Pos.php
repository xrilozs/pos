<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  public function login(){
		$data['title']        = "Login";
		$data['page']         = "login";

		$this->load->view('pages/login', $data);
  }
}

?>