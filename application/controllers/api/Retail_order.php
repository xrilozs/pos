<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Retail_order extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/retail-order/report [GET]
    function get_retail_order_report(){
        #init req & resp
        $resp_obj           = new Response_api();
        $start_date         = $this->input->get('start_date');
        $end_date           = $this->input->get('end_date');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/report [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get retail_order
        $retail_orders      = $this->retail_order_model->get_retail_order_report($start_date, $end_date);
        logging('debug', 'LAST QUERY', $this->db->last_query());
        $retail_order_total = $this->retail_order_model->count_retail_order_report($start_date, $end_date);
        logging('debug', 'LAST QUERY', $this->db->last_query());
        $total_sales        = floatval(0);
        $discount_total     = floatval(0);
        $cash_payment       = floatval(0);
        $cc_payment         = floatval(0);
        foreach ($retail_orders as $order) {
            $total_sales    += $order->total;
            $discount_total += $order->discount;
            if($order->payment_type == 'CASH'){
                $cash_payment += $order->total;
            }else{
                $cc_payment += $order->total;
            }
        }

        $response = array(
            "total_order"       => $retail_order_total,
            "total_sales"       => $total_sales,
            "discount_total"    => $discount_total,
            "cash_payment"      => $cash_payment,
            "cc_payment"        => $cc_payment,
        );

        #response
        logging('debug', '/api/retail-order/report [GET] - Get retail order report is success', $response);
        $resp_obj->set_response(200, "success", "Get retail order report is success", $response);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-order/history [GET]
    function get_retail_order_history(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $sort           = $this->input->get('sort');
        $order_by       = $this->input->get('order_by');  
        $date           = $this->input->get('date');  
        $payment_type   = $this->input->get('payment_type');  
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size, $sort, $order_by);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-order/history [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/retail-order/history [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get retail_discount
        $start                  = $page_number * $page_size;
        $order                  = array('field'=>$order_by, 'order'=>$sort);
        $limit                  = array('start'=>$start, 'size'=>$page_size);
        $retail_order_history   = $this->retail_order_model->get_retail_order_history($search, $date, $payment_type, $order, $limit);
        $records_total          = $this->retail_order_model->count_retail_order_history($search, $date, $payment_type);
        $records_filtered       = $records_total;

        foreach ($retail_order_history as &$item) {
            $item->items = $this->retail_order_item_model->get_retail_order_item_by_order_id($item->id);
        }
        
        #response
        if(empty($draw)){
          logging('debug', '/api/retail-order/history [GET] - Get retail discount is success');
          $resp->set_response(200, "success", "Get retail discount is success", $retail_order_history);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/api/retail-order/history [GET] - Get retail discount is success');
          $resp->set_response_datatable(200, $retail_order_history, $draw, $records_total, $records_filtered);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /api/retail-order/by-id/$id [GET]
    function get_retail_order_by_id($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get retail_order detail
        $retail_order = $this->retail_order_model->get_retail_order_by_id($id);
        if(is_null($retail_order)){
            logging('error', '/api/retail-order/by-id/'.$id.' [GET] - retail_order not found');
            $resp_obj->set_response(404, "failed", "retail_order not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/retail-order/by-id/'.$id.' [GET] - Get retail order by id success', $retail_order);
        $resp_obj->set_response(200, "success", "Get retail order by id success", $retail_order);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-order [POST]
    function create_retail_order(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $admin = $resp['data'];
        
        #check request params
        $keys = array('discount', 'subtotal', 'total_pay', 'order_items', 'payment_type');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-order [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }
        $order_items = $request['order_items'];
        foreach ($order_items as $item) {
            $item_keys = array('retail_item_id', 'qty', 'price');
            if(!check_parameter_by_keys($item, $item_keys)){
                logging('error', '/api/retail-order [POST] - Missing parameter. please check API documentation', $request);
                $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #create retail_order
        $order_id   = get_uniq_id();
        $total      = floatval($request['subtotal']) - floatval($request['discount']);
        $exchange   = $request['total_pay'] - $total;
        $invoice_number = "ORDER-" . strval(time());
        $order      = array(
            "id"            => $order_id,
            "order_number"  => $invoice_number,
            "discount"      => $request['discount'],
            "subtotal"      => $request['subtotal'],
            "total"         => $total,
            "total_pay"     => $request['total_pay'],
            "exchange"      => $exchange,
            "payment_type"  => $request['payment_type'],
            "status"        => "PAID",
            "admin_id"      => $admin->id,
            "created_at"    => date('Y-m-d H:i:s')
        );
        $order_items = [];
        foreach ($request['order_items'] as $item) {
            $order_item = array(
                "id"                => get_uniq_id(),
                "retail_order_id"   => $order_id,
                "retail_item_id"    => $item['retail_item_id'],
                "qty"               => $item['qty'],
                "price"             => $item['price'],
                "name"              => $item['name'],
                "amount"            => floatval($item['price']) * intval($item['qty']),
                "created_at"        => date('Y-m-d H:i:s')
            );
            $this->retail_item_model->update_stock_item($item['retail_item_id'], $item['qty']);
            array_push($order_items, $order_item);
        }
        $flag = $this->retail_order_model->create_retail_order($order, $order_items);
        
        #response
        if(!$flag){
            logging('error', '/api/retail-order [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        print_invoice($invoice_number, $order_items, $total, $request['discount'], $request['subtotal'], $request['total_pay'], $exchange);
        // logging('debug', '/api/retail-order [POST] - Create retail order success', $request);
        // $resp_obj->set_response(200, "success", "Create retail order success", array($order, $order_items));
        // set_output($resp_obj->get_response());
        // return;
    }

    #path: /api/retail-order [PUT]
    function update_retail_order(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'retail_category_id', 'stock_qty', 'barcode', 'price', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-order [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check retail_order
        $retail_order = $this->retail_order_model->get_retail_order_by_id($request['id']);
        if(is_null($retail_order)){
            logging('error', '/api/retail-order [PUT] - retail_order not found', $request);
            $resp_obj->set_response(404, "failed", "retail_order not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update retail_order
        $flag = $this->retail_order_model->update_retail_order($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-order [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/retail-order [PUT] - Update retail order success', $request);
        $resp_obj->set_response(200, "success", "Update retail order success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-order/$id [DELETE]
    function delete_retail_order($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check retail_order
        $retail_order = $this->retail_order_model->get_retail_order_by_id($id);
        if(is_null($retail_order)){
            logging('error', '/api/retail-order/delete/'.$id.' [DELETE] - retail order not found');
            $resp_obj->set_response(404, "failed", "retail order not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update retail_order
        $flag = $this->retail_order_model->delete_retail_order($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-order/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        #remove image
        $remove_res = remove_image($retail_order->img_url);
        logging('debug', '/api/retail-order/delete/'.$id.' [DELETE] - Remove Image Response', $remove_res);

        logging('debug', '/api/retail-order/delete/'.$id.' [DELETE] - Delete retail order success');
        $resp_obj->set_response(200, "success", "Delete retail order success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-order/refund/$id [PUT]
    function refund_retail_order($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/refund/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check retail_order
        $retail_order = $this->retail_order_model->get_retail_order_by_id($id);
        if(is_null($retail_order)){
            logging('error', '/api/retail-order/refund/'.$id.' [PUT] - retail order not found');
            $resp_obj->set_response(404, "failed", "retail order not found");
            set_output($resp_obj->get_response());
            return;
        }

        #refund retail_order
        $flag = $this->retail_order_model->refund_retail_order($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-order/refund/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/retail-order/refund/'.$id.' [PUT] - Refund retail order success');
        $resp_obj->set_response(200, "success", "Refund retail order success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-order/upload-image [POST]
    function upload_retail_order_image(){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/upload-image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/image/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/retail-order/upload-image [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-order/upload-image [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/api/retail-order/upload-image [POST] - Upload retail order image success', $data);
        $resp_obj->set_response(200, "success", "Upload retail order image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
