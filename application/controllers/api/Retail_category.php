<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Retail_category extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/retail-category [GET]
    function get_retail_category(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $sort           = $this->input->get('sort');
        $order_by       = $this->input->get('order_by');  
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size, $sort, $order_by);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/retail-category [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get retail_category
        $start              = $page_number * $page_size;
        $order              = array('field'=>$order_by, 'order'=>$sort);
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $retail_category    = $this->retail_category_model->get_retail_categories($search, $order, $limit);
        $records_total      = $this->retail_category_model->count_retail_category($search);
        $records_filtered   = $records_total;
        
        #response
        if(empty($draw)){
          logging('debug', '/api/retail-category [GET] - Get retail category is success');
          $resp->set_response(200, "success", "Get retail category is success", $retail_category);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/api/retail-category [GET] - Get retail category is success');
          $resp->set_response_datatable(200, $retail_category, $draw, $records_total, $records_filtered);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /api/retail-category/all [GET]
    function get_retail_category_all(){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get retail_category
        $order              = array('field'=>"sort_no", 'order'=>"ASC");
        $retail_category    = $this->retail_category_model->get_retail_categories(null, $order);
        
        #response
        logging('debug', '/api/retail-category/all [GET] - Get retail category all is success', $retail_category);
        $resp->set_response(200, "success", "Get retail category all is success", $retail_category);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-category/by-id/$id [GET]
    function get_retail_category_by_id($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get retail_category by id
        $retail_category = $this->retail_category_model->get_retail_category_by_id($id);
        if(is_null($retail_category)){
            logging('error', '/api/retail-category/by-id/'.$id.' [GET] - retail category not found');
            $resp->set_response(404, "failed", "retail category not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/retail-category/by-id/'.$id.' [GET] - Get retail category by id success', $retail_category);
        $resp->set_response(200, "success", "Get retail category by id success", $retail_category);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/retail-category [POST]
    function create_retail_category(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'sort_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-category [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create retail_category
        $flag = $this->retail_category_model->create_retail_category($request);
        
        #response
        if(!$flag){
            logging('error', '/api/retail-category [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-category [POST] - Create retail category success', $request);
        $resp->set_response(200, "success", "Create retail category success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-category [PUT]
    function update_retail_category(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'sort_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-category [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check retail_category
        $retail_category = $this->retail_category_model->get_retail_category_by_id($request['id']);
        if(is_null($retail_category)){
            logging('error', '/api/retail-category [PUT] - retail category not found', $request);
            $resp->set_response(404, "failed", "retail category not found");
            set_output($resp->get_response());
            return;
        }

        #update retail_category
        $flag = $this->retail_category_model->update_retail_category($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-category [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-category [PUT] - Update retail category success', $request);
        $resp->set_response(200, "success", "Update retail category success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-category/delete/$id [DELETE]
    function delete_retail_category($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-category/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check retail_category
        $retail_category = $this->retail_category_model->get_retail_category_by_id($id);
        if(is_null($retail_category)){
            logging('error', '/api/retail-category/delete/'.$id.' [DELETE] - retail category not found');
            $resp->set_response(404, "failed", "retail category not found");
            set_output($resp->get_response());
            return;
        }

        #active retail_category
        $flag = $this->retail_category_model->delete_retail_category($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-category/delete/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-category/delete/'.$id.' [DELETE] - delete retail category success');
        $resp->set_response(200, "success", "delete retail category success");
        set_output($resp->get_response());
        return;
    }
}