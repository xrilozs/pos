<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Retail_discount extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/retail-discount [GET]
    function get_retail_discount(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $sort           = $this->input->get('sort');
        $order_by       = $this->input->get('order_by');  
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size, $sort, $order_by);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/retail-discount [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get retail_discount
        $start              = $page_number * $page_size;
        $order              = array('field'=>$order_by, 'order'=>$sort);
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $retail_discount    = $this->retail_discount_model->get_retail_discount($search, $order, $limit);
        $records_total      = $this->retail_discount_model->count_retail_discount($search);
        $records_filtered   = $records_total;
        
        #response
        if(empty($draw)){
          logging('debug', '/api/retail-discount [GET] - Get retail discount is success');
          $resp->set_response(200, "success", "Get retail discount is success", $retail_discount);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/api/retail-discount [GET] - Get retail discount is success');
          $resp->set_response_datatable(200, $retail_discount, $draw, $records_total, $records_filtered);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /api/retail-discount/active [GET]
    function get_retail_discount_active(){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get retail_discount
        $search             = $this->input->get('search');
        $order              = array('field'=>"name", 'order'=>"ASC");
        $retail_discount    = $this->retail_discount_model->get_retail_discount($search, $order);
        
        #response
        logging('debug', '/api/retail-discount/all [GET] - Get retail discount all is success', $retail_discount);
        $resp->set_response(200, "success", "Get retail discount all is success", $retail_discount);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-discount/by-id/$id [GET]
    function get_retail_discount_by_id($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get retail_discount by id
        $retail_discount = $this->retail_discount_model->get_retail_discount_by_id($id);
        if(is_null($retail_discount)){
            logging('error', '/api/retail-discount/by-id/'.$id.' [GET] - retail discount not found');
            $resp->set_response(404, "failed", "retail discount not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/retail-discount/by-id/'.$id.' [GET] - Get retail discount by id success', $retail_discount);
        $resp->set_response(200, "success", "Get retail discount by id success", $retail_discount);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/retail-discount [POST]
    function create_retail_discount(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'type', 'value');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-discount [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create retail_discount
        $flag = $this->retail_discount_model->create_retail_discount($request);
        
        #response
        if(!$flag){
            logging('error', '/api/retail-discount [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-discount [POST] - Create retail discount success', $request);
        $resp->set_response(200, "success", "Create retail discount success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-discount [PUT]
    function update_retail_discount(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'type', 'value');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-discount [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check retail_discount
        $retail_discount = $this->retail_discount_model->get_retail_discount_by_id($request['id']);
        if(is_null($retail_discount)){
            logging('error', '/api/retail-discount [PUT] - retail discount not found', $request);
            $resp->set_response(404, "failed", "retail discount not found");
            set_output($resp->get_response());
            return;
        }

        #update retail_discount
        $flag = $this->retail_discount_model->update_retail_discount($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-discount [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-discount [PUT] - Update retail discount success', $request);
        $resp->set_response(200, "success", "Update retail discount success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/retail-discount/delete/$id [DELETE]
    function delete_retail_discount($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/retail-discount/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check retail_discount
        $retail_discount = $this->retail_discount_model->get_retail_discount_by_id($id);
        if(is_null($retail_discount)){
            logging('error', '/api/retail-discount/delete/'.$id.' [DELETE] - retail discount not found');
            $resp->set_response(404, "failed", "retail discount not found");
            set_output($resp->get_response());
            return;
        }

        #active retail_discount
        $flag = $this->retail_discount_model->delete_retail_discount($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-discount/delete/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/retail-discount/delete/'.$id.' [DELETE] - delete retail discount success');
        $resp->set_response(200, "success", "delete retail discount success");
        set_output($resp->get_response());
        return;
    }
}