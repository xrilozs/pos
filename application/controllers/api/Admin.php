<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Admin extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/admin/login [POST]
    function login(){
        #init req & resp
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('pin');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin/login [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get admin
        $pin    = $request['pin'];
        $admin  = $this->admin_model->get_admin_by_pin($pin);
        if(is_null($admin)){
            logging('error', '/api/admin/login [POST] - admin not found', $request);
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #Create admin token      
        $token_expired  = time() + (12 * 60 * 60); //12 hour
        $payload        = array(
          'pin'     => $pin,
          'exp'     => $token_expired
        );
        #access token
        $access_token   = JWT::encode($payload, ACCESS_TOKEN_SECRET);
        #refresh token
        $payload['exp'] = time() + (24 * 60 * 60); //24 hour
        $refresh_token  = JWT::encode($payload, REFRESH_TOKEN_SECRET);
      
        $response = array(
          'access_token'    => $access_token,
          'expiry'          => date(DATE_ISO8601, $token_expired),
          'refresh_token'   => $refresh_token,
          'role'            => $admin->role,
          'name'            => $admin->name
        );
        
        logging('debug', '/api/admin/login [POST] - admin login success', $response);
        $resp->set_response(200, "success", "admin login success", $response);
        set_output($resp->get_response());
        return;
    }

    #path: admin/refresh [GET]
    function refresh(){
      $resp = new Response_api();

      #check header
      $header = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/api/admin/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SECRET, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/api/admin/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $pin      = $jwt->pin;
      $admin    = $this->admin_model->get_admin_by_pin($pin);

      #check admin exist
      if(is_null($admin)){
        logging('debug', '/api/admin/refresh [GET] - admin not found');
        $resp->set_response(401, "failed", "admin not found");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token
      $token_expired    = time() + (12 * 60 * 60);
      $payload          = array(
        'pin'   => $pin,
        'exp'   => $token_expired
      );
      #access token
      $access_token     = JWT::encode($payload, ACCESS_TOKEN_SECRET);
      #refresh token
      $payload['exp']   = time() + (24 * 60 * 60);
      $refresh_token    = JWT::encode($payload, REFRESH_TOKEN_SECRET);

      $response = array(
        'access_token'  => $access_token,
        'expiry'        => date(DATE_ISO8601, $token_expired),
        'refresh_token' => $refresh_token,
        'role'          => $admin->role,
        'name'          => $admin->name
      );
      
      logging('debug', '/api/admin/refresh [GET] - Refresh admin success', $response);
      $resp->set_response(200, "success", "Refresh admin success", $response);
      set_output($resp->get_response());
      return;
    }

    #path: /api/admin [GET]
    function get_admin(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $sort           = $this->input->get('sort');
        $order_by       = $this->input->get('order_by'); 
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size, $sort, $order_by);
        $allowed_role   = array('ADMIN', 'MANAGER');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/admin [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get admin
        $start              = $page_number * $page_size;
        $order              = array('field'=>$order_by, 'order'=>$sort);
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $admin              = $this->admin_model->get_admins($search, $order, $limit);
        $records_total      = $this->admin_model->count_admin($search);
        $records_filtered   = $records_total;
        
        #response
        if(empty($draw)){
          logging('debug', '/api/admin [GET] - Get admin is success', $admin);
          $resp->set_response(200, "success", "Get admin is success", $admin);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/api/admin [GET] - Get admin is success');
          $resp->set_response_datatable(200, $admin, $draw, $records_total, $records_filtered);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /api/admin/by-id/$id [GET]
    function get_admin_by_id($id){
        $resp           = new Response_api();
        $allowed_role   = array('ADMIN','MANAGER');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get admin by id
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/api/admin/by-id/'.$id.' [GET] - admin not found');
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/admin/by-id/'.$id.' [GET] - Get admin by id success', $admin);
        $resp->set_response(200, "success", "Get admin by id success", $admin);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin/profile [GET]
    function get_profile(){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
  
        #response
        logging('debug', '/api/admin/profile [GET] - Get profile success', $admin);
        $resp->set_response(200, "success", "Get profile success", $admin);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/admin [POST]
    function create_admin(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('ADMIN', 'MANAGER');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
        
        #check request params
        $keys = array('name', 'pin', 'role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $admin = $this->admin_model->get_admin_by_pin($request['pin']);
        if($admin){
            logging('error', '/api/admin [POST] - PIN already used', $request);
            $resp->set_response(400, "failed", "PIN already used");
            set_output($resp->get_response());
            return;
        }

        #create admin
        $flag = $this->admin_model->create_admin($request);
        
        #response
        if(!$flag){
            logging('error', '/api/admin [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/admin [POST] - Create admin success', $request);
        $resp->set_response(200, "success", "Create admin success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin [PUT]
    function update_admin(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('ADMIN', 'MANAGER');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'pin', 'role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/admin [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($request['id']);
        if(is_null($admin)){
            logging('error', '/api/admin [PUT] - admin not found', $request);
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #check changes of PIN
        if($admin->pin != $request['pin']){
            #check duplicate PIN
            $admin_exist = $this->admin_model->get_admin_by_pin($request['pin']);
            if($admin_exist){
                logging('error', '/api/admin [POST] - New PIN already used', $request);
                $resp->set_response(400, "failed", "New PIN already used");
                set_output($resp->get_response());
                return;
            }
        }

        #update admin
        $flag = $this->admin_model->update_admin($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/admin [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/admin [PUT] - Update admin success', $request);
        $resp->set_response(200, "success", "Update admin success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/admin/delete/$id [DELETE]
    function delete($id){
        #init variable
        $resp           = new Response_api();
        $allowed_role   = array('ADMIN', 'MANAGER');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/admin/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/api/admin/delete/'.$id.' [DELETE] - admin not found');
            $resp->set_response(404, "failed", "admin not found");
            set_output($resp->get_response());
            return;
        }

        #active admin
        $flag = $this->admin_model->delete($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/admin/delete/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/admin/delete/'.$id.' [DELETE] - delete admin success');
        $resp->set_response(200, "success", "delete admin success");
        set_output($resp->get_response());
        return;
    }
}