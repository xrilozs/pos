<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Resto_menu extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/resto-menu [GET]
    function get_resto_menu(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $sort               = $this->input->get('sort');
        $order_by           = $this->input->get('order_by'); 
        $resto_category_id  = $this->input->get('resto_category_id');
        $draw               = $this->input->get('draw');
        $params             = array($sort, $order_by);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/resto-menu [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get resto_menu
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = null;
        if($page_number && $page_size){
            $start = $page_number * $page_size;
            $limit = array('start'=>$start, 'size'=>$page_size);
        }
        $resto_menu         = $this->resto_menu_model->get_resto_menu($search, $resto_category_id, $order, $limit);
        $records_total      = $this->resto_menu_model->count_resto_menu($search, $resto_category_id);
        $records_filtered   = $records_total;

        #response
        if(empty($draw)){
            logging('debug', '/api/resto-menu [GET] - Get resto_menu is success', $resto_menu);
            $resp_obj->set_response(200, "success", "Get resto_menu is success", $resto_menu);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/resto-menu [GET] - Get resto_menu is success');
            $resp_obj->set_response_datatable(200, $resto_menu, $draw, $records_total, $records_filtered);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/resto-menu/by-id/$id [GET]
    function get_resto_menu_by_id($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get resto_menu detail
        $resto_menu = $this->resto_menu_model->get_resto_menu_by_id($id);
        if(is_null($resto_menu)){
            logging('error', '/api/resto-menu/by-id/'.$id.' [GET] - resto menu not found');
            $resp_obj->set_response(404, "failed", "resto menu not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/resto-menu/by-id/'.$id.' [GET] - Get resto menu by id success', $resto_menu);
        $resp_obj->set_response(200, "success", "Get resto menu by id success", $resto_menu);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/resto-menu [POST]
    function create_resto_menu(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'resto_category_id', 'type', 'price', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/resto-menu [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #create resto_menu
        $flag = $this->resto_menu_model->create_resto_menu($request);
        
        #response
        if(!$flag){
            logging('error', '/api/resto-menu [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/resto-menu [POST] - Create resto menu success', $request);
        $resp_obj->set_response(200, "success", "Create resto menu success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/resto-menu [PUT]
    function update_resto_menu(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'resto_category_id', 'type', 'price', 'img_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/resto-menu [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check resto_menu
        $resto_menu = $this->resto_menu_model->get_resto_menu_by_id($request['id']);
        if(is_null($resto_menu)){
            logging('error', '/api/resto-menu [PUT] - resto menu not found', $request);
            $resp_obj->set_response(404, "failed", "resto menu not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update resto_menu
        $flag = $this->resto_menu_model->update_resto_menu($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/resto-menu [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/resto-menu [PUT] - Update resto menu success', $request);
        $resp_obj->set_response(200, "success", "Update resto menu success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/resto-menu/$id [DELETE]
    function delete_resto_menu($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check resto_menu
        $resto_menu = $this->resto_menu_model->get_resto_menu_by_id($id);
        if(is_null($resto_menu)){
            logging('error', '/api/resto-menu/delete/'.$id.' [DELETE] - resto menu not found');
            $resp_obj->set_response(404, "failed", "resto menu not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update resto_menu
        $flag = $this->resto_menu_model->delete_resto_menu($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/resto-menu/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        #remove image
        $remove_res = remove_image($resto_menu->img_url);
        logging('debug', '/api/resto-menu/delete/'.$id.' [DELETE] - Remove Image Response', $remove_res);

        logging('debug', '/api/resto-menu/delete/'.$id.' [DELETE] - Delete resto menu success');
        $resp_obj->set_response(200, "success", "Delete resto menu success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/resto-menu/upload-image [POST]
    function upload_resto_menu_image(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu/upload-image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/image/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/resto-menu/upload-image [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/resto-menu/upload-image [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/api/resto-menu/upload-image [POST] - Upload resto menu image success', $data);
        $resp_obj->set_response(200, "success", "Upload resto menu image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
