<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Retail_item extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/retail-item [GET]
    function get_retail_item(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $sort               = $this->input->get('sort');
        $order_by           = $this->input->get('order_by'); 
        $retail_category_id = $this->input->get('retail_category_id');
        $draw               = $this->input->get('draw');
        $params             = array($sort, $order_by);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/retail-item [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get retail_item
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = null;
        if(!is_null($page_number) && $page_size){
            $start = $page_number * $page_size;
            $limit = array('start'=>$start, 'size'=>$page_size);
        }
        $retail_item        = $this->retail_item_model->get_retail_item($search, $retail_category_id, $order, $limit);
        $query = $this->db->last_query();
        $records_total      = $this->retail_item_model->count_retail_item($search, $retail_category_id);
        $records_filtered   = $records_total;

        #response
        if(empty($draw)){
            logging('debug', '/api/retail-item [GET] - Get retail item is success', $retail_item);
            $resp_obj->set_response(200, "success", "Get retail item is success", $retail_item);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/retail-item [GET] - Get retail item is success');
            $resp_obj->set_response_datatable(200, $retail_item, $draw, $records_total, $records_total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/retail-item/by-id/$id [GET]
    function get_retail_item_by_id($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get retail_item detail
        $retail_item = $this->retail_item_model->get_retail_item_by_id($id);
        if(is_null($retail_item)){
            logging('error', '/api/retail-item/by-id/'.$id.' [GET] - retail_item not found');
            $resp_obj->set_response(404, "failed", "retail_item not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/retail-item/by-id/'.$id.' [GET] - Get retail item by id success', $retail_item);
        $resp_obj->set_response(200, "success", "Get retail item by id success", $retail_item);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-item [POST]
    function create_retail_item(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'retail_category_id', 'stock_qty', 'price', 'img_url', 'expired_date');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-item [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #create retail_item
        $flag = $this->retail_item_model->create_retail_item($request);
        
        #response
        if(!$flag){
            logging('error', '/api/retail-item [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/retail-item [POST] - Create retail item success', $request);
        $resp_obj->set_response(200, "success", "Create retail item success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-item [PUT]
    function update_retail_item(){
        #check token
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'retail_category_id', 'stock_qty', 'price', 'img_url', 'expired_date');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/retail-item [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check retail_item
        $retail_item = $this->retail_item_model->get_retail_item_by_id($request['id']);
        if(is_null($retail_item)){
            logging('error', '/api/retail-item [PUT] - retail_item not found', $request);
            $resp_obj->set_response(404, "failed", "retail_item not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update retail_item
        $flag = $this->retail_item_model->update_retail_item($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-item [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/retail-item [PUT] - Update retail item success', $request);
        $resp_obj->set_response(200, "success", "Update retail item success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-item/$id [DELETE]
    function delete_retail_item($id){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check retail_item
        $retail_item = $this->retail_item_model->get_retail_item_by_id($id);
        if(is_null($retail_item)){
            logging('error', '/api/retail-item/delete/'.$id.' [DELETE] - retail item not found');
            $resp_obj->set_response(404, "failed", "retail item not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update retail_item
        $flag = $this->retail_item_model->delete_retail_item($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/retail-item/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }

        #remove image
        $remove_res = remove_image($retail_item->img_url);
        logging('debug', '/api/retail-item/delete/'.$id.' [DELETE] - Remove Image Response', $remove_res);

        logging('debug', '/api/retail-item/delete/'.$id.' [DELETE] - Delete retail item success');
        $resp_obj->set_response(200, "success", "Delete retail item success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/retail-item/upload-image [POST]
    function upload_retail_item_image(){
        #check token
        $resp_obj   = new Response_api();
        $header     = $this->input->request_headers();
        $resp       = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item/upload-image [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/image/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/retail-item/upload-image [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = upload_image($file, $destination, false);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/retail-item/upload-image [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
        $data['full_img_url'] = BASE_URL . $data['img_url'];
        logging('debug', '/api/retail-item/upload-image [POST] - Upload retail item image success', $data);
        $resp_obj->set_response(200, "success", "Upload retail item image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}
