<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Resto_category extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/resto-category [GET]
    function get_resto_category(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $sort           = $this->input->get('sort');
        $order_by       = $this->input->get('order_by');  
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size, $sort, $order_by);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/resto-category [GET] - Missing parameter. please check API documentation");
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get resto_category
        $start              = $page_number * $page_size;
        $order              = array('field'=>$order_by, 'order'=>$sort);
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $resto_category     = $this->resto_category_model->get_resto_categories($search, $order, $limit);
        $records_total      = $this->resto_category_model->count_resto_category($search);
        $records_filtered   = $records_total;
        
        #response
        if(empty($draw)){
          logging('debug', '/api/resto-category [GET] - Get resto category is success');
          $resp->set_response(200, "success", "Get resto category is success", $resto_category);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/api/resto-category [GET] - Get resto category is success');
          $resp->set_response_datatable(200, $resto_category, $draw, $records_total, $records_filtered);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /api/resto-category/all [GET]
    function get_resto_category_all(){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get resto_category
        $order      = array('field'=>"sort_no", 'order'=>"ASC");
        $resto_category   = $this->resto_category_model->get_resto_categories(null, $order);
        
        #response
        logging('debug', '/api/resto-category/all [GET] - Get resto category all is success');
        $resp->set_response(200, "success", "Get resto category all is success", $resto_category);
        set_output($resp->get_response());
        return;
    }

    #path: /api/resto-category/by-id/$id [GET]
    function get_resto_category_by_id($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get resto_category by id
        $resto_category = $this->resto_category_model->get_resto_category_by_id($id);
        if(is_null($resto_category)){
            logging('error', '/api/resto-category/by-id/'.$id.' [GET] - resto category not found');
            $resp->set_response(404, "failed", "resto category not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/api/resto-category/by-id/'.$id.' [GET] - Get resto category by id success', $resto_category);
        $resp->set_response(200, "success", "Get resto category by id success", $resto_category);
        set_output($resp->get_response());
        return;
    }
  
    #path: /api/resto-category [POST]
    function create_resto_category(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('name', 'sort_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/resto-category [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create resto_category
        $flag = $this->resto_category_model->create_resto_category($request);
        
        #response
        if(!$flag){
            logging('error', '/api/resto-category [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/resto-category [POST] - Create resto category success', $request);
        $resp->set_response(200, "success", "Create resto category success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/resto-category [PUT]
    function update_resto_category(){
        #check token
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name', 'sort_no');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/resto-category [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check resto_category
        $resto_category = $this->resto_category_model->get_resto_category_by_id($request['id']);
        if(is_null($resto_category)){
            logging('error', '/api/resto-category [PUT] - resto category not found', $request);
            $resp->set_response(404, "failed", "resto category not found");
            set_output($resp->get_response());
            return;
        }

        #update resto_category
        $flag = $this->resto_category_model->update_resto_category($request);
        
        #response
        if(empty($flag)){
            logging('error', '/api/resto-category [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/resto-category [PUT] - Update resto category success', $request);
        $resp->set_response(200, "success", "Update resto category success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/resto-category/delete/$id [DELETE]
    function delete_resto_category($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/resto-category/delete/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check resto_category
        $resto_category = $this->resto_category_model->get_resto_category_by_id($id);
        if(is_null($resto_category)){
            logging('error', '/api/resto-category/delete/'.$id.' [DELETE] - resto_category not found');
            $resp->set_response(404, "failed", "resto category not found");
            set_output($resp->get_response());
            return;
        }

        #active resto_category
        $flag = $this->resto_category_model->delete_resto_category($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/resto-category/delete/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/resto-category/delete/'.$id.' [DELETE] - delete resto category success');
        $resp->set_response(200, "success", "delete resto category success");
        set_output($resp->get_response());
        return;
    }
}