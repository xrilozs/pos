<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require __DIR__ . '/../../vendor/autoload.php';
require_once('./vendor/autoload.php');
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\EscposImage;

class Common extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  function index(){
    redirect(base_url('resto/point-of-sales'));
  }

  function print(){
		// $this->load->view('pages/print');
    try {
      // Enter the share name for your USB printer here
      // $connector = null;
      $connector = new WindowsPrintConnector("POS58 Printer");

      /* Print a "Hello world" receipt" */
      $printer = new Printer($connector);
      #Information
      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer -> setTextSize(2, 4);
      $printer -> text("Sendok Garpu\n");
      $printer -> text("Indonesian\n");
      $printer -> text("Grocery Shop\n");
      $printer -> feed();
      $printer -> selectPrintMode(Printer::MODE_FONT_A);
      $printer -> text("Shop 6/97 Elizabeth Street\n");
      $printer -> text("Brisbane City, Queensland 4000\n");
      $printer -> feed();
      $printer -> text("ABN 42 167 986 772\n");
      $printer -> text("Phone : (07) 4602 9979\n");
      $printer -> text("www.sendokgarpu.com.au\n");
      $printer -> feed(2);
      #Total
      $printer -> text("Total\n");
      $printer -> text(str_repeat("=", 30) . "\n");
      $printer -> setTextSize(2, 4);
      $printer -> text("AUD10.00\n");
      $printer -> selectPrintMode(Printer::MODE_FONT_A);
      $printer -> text(str_repeat("=", 30) . "\n");
      $printer -> feed(2);

      #Items Header
      $printer -> setJustification(Printer::JUSTIFY_LEFT);
      $printer -> selectPrintMode(Printer::MODE_FONT_B);      
      $printer->text("Items");
      $printer->text(str_repeat(" ", 20));
      $printer->text("Price");
      $printer->text("\n");

      $printer -> text(str_repeat("-", 30) . "\n");
      $groceryList = [
        array("name" => "Item #1", "price" => "AUD10.00"),
        array("name" => "Barang #2", "price" => "AUD11.00"),
        array("name" => "Goods or stuff #1", "price" => "AUD12.00"),
      ];

      //Items list
      foreach ($groceryList as $row) {
        $textItem = $row['name'];
        $textPrice = $row['price'];
        $textItemSize = strlen($textItem);
        $textPriceSize = strlen($textPrice);

        if($textItemSize > 15){
          $chunks = str_split($textItem, 15);
          $textItem = implode("\n", $chunks);
          $textItemSize = strlen($chunks[sizeof($chunks)-1]);
        }
        $blankSize = 30 - ($textItemSize + $textPriceSize);
        $printer -> text($textItem);
        $printer -> text(str_repeat(" ", $blankSize));
        $printer -> text($textPrice);
        $printer->text("\n");
      }

      $printer -> text(str_repeat(".", 30));
      $printer -> feed();

      #Subtotal
      $subtotal = "AUD33.00";
      $blankSize = 30-8-strlen($subtotal);
      $printer->text("Subtotal");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($subtotal);
      $printer -> feed();
      #Diskon
      $diskon = "AUD3.00";
      $blankSize = 30-6-strlen($diskon);
      $printer->text("Diskon");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($diskon);
      $printer -> feed();
      #Total
      $total = "AUD30.00";
      $blankSize = 30-5-strlen($total);
      $printer->text("Total");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($total);
      $printer -> feed();

      $printer -> text(str_repeat(".", 30));
      $printer -> feed();

      $printer -> selectPrintMode(Printer::MODE_FONT_A);      
      #Cash
      $cash = "AUD30.00";
      $blankSize = 30-4-strlen($cash);
      $printer->text("Cash");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($cash);
      $printer -> feed();
      #Cash
      $change = "AUD0.00";
      $blankSize = 30-10-strlen($change);
      $printer->text("Change due");
      $printer->text(str_repeat(" ", $blankSize));
      $printer->text($change);
      $printer -> feed();

      $printer -> text(str_repeat("-", 30));
      $printer -> feed();

      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer->text("Jul 24, 2023, 03:00 PM\n");
      $printer->text("Staff at Register: Grocery SG\n");
      $printer->text("Receipt: #1-1231-1");
      $printer -> feed();

      $printer -> cut();    
      $printer -> close();  
    } catch (Exception $e) {
        echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
    }
  }

  function print_html(){
    $connector = new NetworkPrintConnector("ws://127.0.0.1", 40213);
    $printer = new Printer($connector);
    $printer->text("Receipt: #1-1231-1");
    $printer -> feed();

    $printer -> cut();    
    $printer -> close();  
  }
}