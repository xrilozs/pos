<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translateUri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translateUri_dashes'] = FALSE;

$route['print']['GET']           = 'common/print';
$route['print-html']['GET']           = 'common/print_html';

/* API */
#admin
$route['api/admin/login']['POST']           = 'api/admin/login';
$route['api/admin/refresh']['GET']          = 'api/admin/refresh';
$route['api/admin']['GET']                  = 'api/admin/get_admin';
$route['api/admin/by-id/(:any)']['GET']     = 'api/admin/get_admin_by_id/$1';
$route['api/admin/profile']['GET']          = 'api/admin/get_profile';
$route['api/admin']['POST']                 = 'api/admin/create_admin';
$route['api/admin']['PUT']                  = 'api/admin/update_admin';
$route['api/admin/delete/(:any)']['DELETE'] = 'api/admin/delete/$1';

#resto category
$route['api/resto-category']['GET']                   = 'api/resto_category/get_resto_category';
$route['api/resto-category/all']['GET']               = 'api/resto_category/get_resto_category_all';
$route['api/resto-category/by-id/(:any)']['GET']      = 'api/resto_category/get_resto_category_by_id/$1';
$route['api/resto-category']['POST']                  = 'api/resto_category/create_resto_category';
$route['api/resto-category']['PUT']                   = 'api/resto_category/update_resto_category';
$route['api/resto-category/delete/(:any)']['DELETE']  = 'api/resto_category/delete_resto_category/$1';
$route['api/resto-category/count']['GET']             = 'api/resto_category/get_resto_category_count';

#resto menu
$route['api/resto-menu']['GET']                   = 'api/resto_menu/get_resto_menu';
$route['api/resto-menu/by-id/(:any)']['GET']      = 'api/resto_menu/get_resto_menu_by_id/$1';
$route['api/resto-menu']['POST']                  = 'api/resto_menu/create_resto_menu';
$route['api/resto-menu']['PUT']                   = 'api/resto_menu/update_resto_menu';
$route['api/resto-menu/delete/(:any)']['DELETE']  = 'api/resto_menu/delete_resto_menu/$1';
$route['api/resto-menu/upload-image']['POST']     = 'api/resto_menu/upload_resto_menu_image';

#retail category
$route['api/retail-category']['GET']                   = 'api/retail_category/get_retail_category';
$route['api/retail-category/all']['GET']               = 'api/retail_category/get_retail_category_all';
$route['api/retail-category/by-id/(:any)']['GET']      = 'api/retail_category/get_retail_category_by_id/$1';
$route['api/retail-category']['POST']                  = 'api/retail_category/create_retail_category';
$route['api/retail-category']['PUT']                   = 'api/retail_category/update_retail_category';
$route['api/retail-category/delete/(:any)']['DELETE']  = 'api/retail_category/delete_retail_category/$1';
$route['api/retail-category/count']['GET']             = 'api/retail_category/get_retail_category_count';

#retail item
$route['api/retail-item']['GET']                   = 'api/retail_item/get_retail_item';
$route['api/retail-item/by-id/(:any)']['GET']      = 'api/retail_item/get_retail_item_by_id/$1';
$route['api/retail-item']['POST']                  = 'api/retail_item/create_retail_item';
$route['api/retail-item']['PUT']                   = 'api/retail_item/update_retail_item';
$route['api/retail-item/delete/(:any)']['DELETE']  = 'api/retail_item/delete_retail_item/$1';
$route['api/retail-item/upload-image']['POST']     = 'api/retail_item/upload_retail_item_image';

#retail discount
$route['api/retail-discount']['GET']                   = 'api/retail_discount/get_retail_discount';
$route['api/retail-discount/active']['GET']            = 'api/retail_discount/get_retail_discount_active';
$route['api/retail-discount/by-id/(:any)']['GET']      = 'api/retail_discount/get_retail_discount_by_id/$1';
$route['api/retail-discount']['POST']                  = 'api/retail_discount/create_retail_discount';
$route['api/retail-discount']['PUT']                   = 'api/retail_discount/update_retail_discount';
$route['api/retail-discount/delete/(:any)']['DELETE']  = 'api/retail_discount/delete_retail_discount/$1';

#retail order
$route['api/retail-order']['POST']              = 'api/retail_order/create_retail_order';
$route['api/retail-order/report']['GET']        = 'api/retail_order/get_retail_order_report';
$route['api/retail-order/history']['GET']       = 'api/retail_order/get_retail_order_history';
$route['api/retail-order/refund/(:any)']['PUT'] = 'api/retail_order/refund_retail_order/$1';
$route['api/retail-order/print']['GET']         = 'api/retail_order/test_print';

/* API */

/* WEB */
$route['login']                 = 'web/pos/login';
$route['admin']                 = 'web/pos/admin';
#resto
$route['resto/point-of-sales']  = 'web/resto/point_of_sales';
$route['resto/held-order']      = 'web/resto/held_order';
$route['resto/order-history']   = 'web/resto/order_history';
$route['resto/report']          = 'web/resto/report';
$route['resto/category']        = 'web/resto/category';
$route['resto/menu']            = 'web/resto/menu';
$route['resto/discount']        = 'web/resto/discount';
#retail
$route['retail/point-of-sales']  = 'web/retail/point_of_sales';
$route['retail/order-history']   = 'web/retail/order_history';
$route['retail/report']          = 'web/retail/report';
$route['retail/category']        = 'web/retail/category';
$route['retail/item']            = 'web/retail/item';
$route['retail/discount']        = 'web/retail/discount';
/* WEB */
