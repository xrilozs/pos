<?php
  class Admin_model extends CI_Model{
    public $pin;
    public $name;
    public $email;
    public $phone;
    public $role;
    
    function get_admin_by_pin($pin){
      $this->db->where("pin", $pin);
      $query = $this->db->get('admin');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_admins($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          pin LIKE '%$search%' OR
          email LIKE '%$search%' OR
          phone LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('admin');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }


    function count_admin($search=null){
      $this->db->from('admin');
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          pin LIKE '%$search%' OR
          email LIKE '%$search%' OR
          phone LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_admin_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('admin');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_admin($data){
      $this->pin        = $data['pin'];
      $this->name       = $data['name'];
      $this->email      = array_key_exists("email", $data) ? $data['email'] : null;
      $this->phone      = array_key_exists("phone", $data) ? $data['phone'] : null;
      $this->role       = $data['role'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('admin', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_admin($data){
      $this->pin        = $data['pin'];
      $this->name       = $data['name'];
      $this->email      = array_key_exists("email", $data) ? $data['email'] : null;
      $this->phone      = array_key_exists("phone", $data) ? $data['phone'] : null;
      $this->role       = $data['role'];

      $this->db->update('admin', $this, array("id"=>$data['id']));
      return $this->db->affected_rows();
    }

    function delete($id){
      $this->db->where('id', $id);
      $this->db->delete('admin');
      return $this->db->affected_rows();
    }
  }
?>
