<?php
  class Resto_category_model extends CI_Model{
    public $name;
    public $description;
    public $sort_no;

    function get_resto_categories($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          description LIKE '%$search%' OR
          sort_no LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('resto_category');
      return $query->result();
    }

    function count_resto_category($search=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          description LIKE '%$search%' OR
          sort_no LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('resto_category');
      return $this->db->count_all_results();
    }

    function get_resto_category_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('resto_category');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_resto_category($data){
      $this->name         = $data['name'];
      $this->sort_no      = $data['sort_no'];
      $this->description  = array_key_exists("description", $data) ? $data['description'] : null;
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('resto_category', $this);
      return $this->db->affected_rows();
    }

    function update_resto_category($data){
      $this->id           = $data['id'];
      $this->name         = $data['name'];
      $this->sort_no      = $data['sort_no'];
      $this->description  = array_key_exists("description", $data) ? $data['description'] : null;

      $this->db->update('resto_category', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_resto_category($id){
      $this->db->where('id', $id);
      $this->db->delete('resto_category');
      return $this->db->affected_rows();
    }
  }
?>
