<?php
  class Retail_category_model extends CI_Model{
    public $name;
    public $description;
    public $sort_no;

    function get_retail_categories($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          description LIKE '%$search%' OR
          sort_no LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('retail_category');
      return $query->result();
    }

    function count_retail_category($search=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          description LIKE '%$search%' OR
          sort_no LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('retail_category');
      return $this->db->count_all_results();
    }

    function get_retail_category_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('retail_category');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_retail_category($data){
      $this->name         = $data['name'];
      $this->sort_no      = $data['sort_no'];
      $this->description  = array_key_exists("description", $data) ? $data['description'] : null;
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('retail_category', $this);
      return $this->db->affected_rows();
    }

    function update_retail_category($data){
      $this->id           = $data['id'];
      $this->name         = $data['name'];
      $this->sort_no      = $data['sort_no'];
      $this->description  = array_key_exists("description", $data) ? $data['description'] : null;

      $this->db->update('retail_category', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_retail_category($id){
      $this->db->where('id', $id);
      $this->db->delete('retail_category');
      return $this->db->affected_rows();
    }
  }
?>
