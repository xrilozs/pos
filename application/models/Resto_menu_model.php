<?php
  class Resto_menu_model extends CI_Model{
    public $name;
    public $resto_category_id;
    public $type;
    public $description;
    public $price;
    public $img_url;

    function get_resto_menu($search=null, $resto_category_id=null, $order=null, $limit=null){
      $this->db->select("m.*, c.name as category_name,
        CONCAT('" . BASE_URL . "', m.img_url) as full_img_url
      ");
      if($search){
        $where_search = "(
          m.name LIKE '%$search%' OR 
          c.name LIKE '%$search%' OR
          m.type LIKE '%$search%' OR
          m.description LIKE '%$search%' OR
          m.price LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($resto_category_id){
        $this->db->where("m.resto_category_id", $resto_category_id);
      }
      if($order){
        $this->db->order_by("m.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from('resto_menu m');
      $this->db->join('resto_category c', 'c.id = m.resto_category_id');
      $query = $this->db->get();
      return $query->result();
    }

    function count_resto_menu($search=null, $resto_category_id=null){
      $this->db->from('resto_menu m');
      $this->db->join('resto_category c', 'c.id = m.resto_category_id');
      if($search){
        $where_search = "(
          m.name LIKE '%$search%' OR 
          c.name LIKE '%$search%' OR
          m.type LIKE '%$search%' OR
          m.description LIKE '%$search%' OR
          m.price LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_resto_menu_by_id($id){
      $this->db->select("m.*, c.name as category_name,
        CONCAT('" . BASE_URL . "', m.img_url) as full_img_url
      ");
      $this->db->where("m.id", $id);
      $this->db->from('resto_menu m');
      $this->db->join('resto_category c', 'c.id = m.resto_category_id');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_resto_menu($data){
      $this->name               = $data['name'];
      $this->resto_category_id  = $data['resto_category_id'];
      $this->type               = $data['type'];
      $this->description        = array_key_exists("description", $data) ? $data['description'] : null;
      $this->price              = $data['price'];
      $this->img_url            = $data['img_url'];
      $this->created_at         = date('Y-m-d H:i:s');

      $this->db->insert('resto_menu', $this);
      return $this->db->affected_rows();
    }

    function update_resto_menu($data){
      $id = $data['id'];
      $this->name               = $data['name'];
      $this->resto_category_id  = $data['resto_category_id'];
      $this->type               = $data['type'];
      $this->description        = array_key_exists("description", $data) ? $data['description'] : null;
      $this->price              = $data['price'];
      $this->img_url            = $data['img_url'];

      $this->db->update('resto_menu', $this, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function delete_resto_menu($id){
      $this->db->where('id', $id);
      $this->db->delete('resto_menu');
      return $this->db->affected_rows();
    }
  }
?>
