<?php
  class Retail_order_item_model extends CI_Model{
    function get_retail_order_item_by_order_id($order_id){
      $this->db->select("oi.*, i.name as item_name");
      $this->db->where("retail_order_id", $order_id);
      $this->db->join("retail_item i", "i.id = oi.retail_item_id", "LEFT");
      $query = $this->db->get('retail_order_item oi');
      return $query->result();
    }
  }
?>