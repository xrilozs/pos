<?php
  class Retail_discount_model extends CI_Model{
    public $name;
    public $code;
    public $type;
    public $value;
    public $periode_start;
    public $periode_end;

    function get_retail_discount($search=null, $is_active=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          code LIKE '%$search%' OR
          value LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $where_active = "periode_start <= now() && periode_end >= now()";
        $this->db->where($where_active);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('retail_discount');
      return $query->result();
    }

    function count_retail_discount($search=null, $is_active=null){
      if($search){
        $where_search = "(
          name LIKE '%$search%' OR 
          code LIKE '%$search%' OR
          value LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if(!is_null($is_active)){
        $where_active = "periode_start <= now() && periode_end >= now()";
        $this->db->where($where_active);
      }
      $this->db->from('retail_discount');
      return $this->db->count_all_results();
    }

    function get_retail_discount_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('retail_discount');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_retail_discount($data){
      $this->name         = $data['name'];
      $this->code         = $data['code'];
      $this->type         = $data['type'];
      $this->value        = $data['value'];
      $this->periode_start= $data['periode_start'];
      $this->periode_end  = $data['periode_end'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('retail_discount', $this);
      return $this->db->affected_rows();
    }

    function update_retail_discount($data){
      $this->id           = $data['id'];
      $this->name         = $data['name'];
      $this->code         = $data['code'];
      $this->type         = $data['type'];
      $this->value        = $data['value'];
      $this->periode_start= $data['periode_start'];
      $this->periode_end  = $data['periode_end'];

      $this->db->update('retail_discount', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_retail_discount($id){
      $this->db->where('id', $id);
      $this->db->delete('retail_discount');
      return $this->db->affected_rows();
    }
  }
?>
