<?php
  class Retail_order_model extends CI_Model{
    function get_retail_order_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('retail_order');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_retail_order_report($start_date, $end_date){
      $report_range = "created_at >= '$start_date' && created_at <= '$end_date'";
      $this->db->where($report_range);
      $this->db->from('retail_order');
      $query = $this->db->get();
      return $query->result();
    }

    function count_retail_order_report($start_date, $end_date){
      $report_range = "created_at >= '$start_date' && created_at <= '$end_date'";
      $this->db->where($report_range);
      $this->db->from('retail_order');
      return $this->db->count_all_results();
    }

    function get_retail_order_history($search=null, $date=null, $payment_type=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          order_number LIKE '%$search%' OR 
          payment_type LIKE '%$search%' OR
          total LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($date){
        $where_date = "DATE(created_at) = '$date'";
        $this->db->where($where_date);
      }
      if($payment_type){
        $this->db->where("payment_type", $payment_type);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('retail_order');
      return $query->result();
    }

    function count_retail_order_history($search=null, $date=null, $payment_type=null){
      if($search){
        $where_search = "(
          order_number LIKE '%$search%' OR 
          payment_type LIKE '%$search%' OR
          total LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($date){
        $where_date = "DATE(created_at) = '$date'";
        $this->db->where($where_date);
      }
      if($payment_type){
        $this->db->where("payment_type", $payment_type);
      }
      $this->db->from('retail_order');
      return $this->db->count_all_results();
    }

    function create_retail_order($order, $order_items){
      $this->db->trans_begin();

      $this->db->insert('retail_order', $order);
      foreach ($order_items as $item) {
        unset($item['name']);
        $this->db->insert('retail_order_item', $item);
      }

      if ($this->db->trans_status() === FALSE){
        $this->db->trans_rollback();
        return false;
      }else{
        $this->db->trans_commit();
        return true;
      }
    }

    function refund_retail_order($id){
      $this->db->where('id', $id); 
      $this->db->update('retail_order', array('status' => 'REFUND'));
      return $this->db->affected_rows();
    }
  }
?>