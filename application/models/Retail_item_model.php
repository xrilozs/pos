<?php
  class Retail_item_model extends CI_Model{
    public $name;
    public $retail_category_id;
    public $description;
    public $price;
    public $stock_qty;
    public $img_url;
    public $barcode;
    public $expired_date;

    function get_retail_item($search=null, $retail_category_id=null, $order=null, $limit=null){
      $this->db->select("m.*, c.name as retail_category_name,
        CONCAT('" . BASE_URL . "', m.img_url) as full_img_url
      ");
      if($search){
        $where_search = "(
          m.name LIKE '%$search%' OR 
          c.name LIKE '%$search%' OR
          m.stock_qty LIKE '%$search%' OR
          m.barcode LIKE '%$search%' OR
          m.description LIKE '%$search%' OR
          m.price LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($retail_category_id){
        $this->db->where("m.retail_category_id", $retail_category_id);
      }
      if($order){
        $this->db->order_by("m.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from('retail_item m');
      $this->db->join('retail_category c', 'c.id = m.retail_category_id', 'LEFT');
      $query = $this->db->get();
      return $query->result();
    }

    function count_retail_item($search=null, $retail_category_id=null){
      $this->db->from('retail_item m');
      $this->db->join('retail_category c', 'c.id = m.retail_category_id', 'LEFT');
      if($search){
        $where_search = "(
          m.name LIKE '%$search%' OR 
          c.name LIKE '%$search%' OR
          m.stock_qty LIKE '%$search%' OR
          m.barcode LIKE '%$search%' OR
          m.description LIKE '%$search%' OR
          m.price LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      return $this->db->count_all_results();
    }

    function get_retail_item_by_id($id){
      $this->db->select("m.*, c.name as retail_category_name,
        CONCAT('" . BASE_URL . "', m.img_url) as full_img_url
      ");
      $this->db->where("m.id", $id);
      $this->db->from('retail_item m');
      $this->db->join('retail_category c', 'c.id = m.retail_category_id', 'LEFT');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_retail_item($data){
      $this->name               = $data['name'];
      $this->retail_category_id = $data['retail_category_id'];
      $this->stock_qty          = $data['stock_qty'];
      $this->barcode            = $data['barcode'];
      $this->description        = array_key_exists("description", $data) ? $data['description'] : null;
      $this->price              = $data['price'];
      $this->img_url            = $data['img_url'];
      $this->expired_date       = $data['expired_date'];
      $this->created_at         = date('Y-m-d H:i:s');

      $this->db->insert('retail_item', $this);
      return $this->db->affected_rows();
    }

    function update_retail_item($data){
      $id = $data['id'];
      $this->name               = $data['name'];
      $this->retail_category_id = $data['retail_category_id'];
      $this->stock_qty          = $data['stock_qty'];
      $this->barcode            = $data['barcode'];
      $this->description        = array_key_exists("description", $data) ? $data['description'] : null;
      $this->price              = $data['price'];
      $this->img_url            = $data['img_url'];
      $this->expired_date       = $data['expired_date'];

      $this->db->update('retail_item', $this, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function delete_retail_item($id){
      $this->db->where('id', $id);
      $this->db->delete('retail_item');
      return $this->db->affected_rows();
    }

    function update_stock_item($id, $new_stock_qty){
      $this->db->where('id', $id); 
      $this->db->set('stock_qty', 'stock_qty - ' . $new_stock_qty, FALSE);
      $this->db->update('retail_item');
      return $this->db->affected_rows();
    }
  }
?>
